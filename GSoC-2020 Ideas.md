Here is a non-exhaustive list of projects which would potentially be suitable for a student over the summer. We highly encourage students to also join our [Users mailing list](https://groups.google.com/forum/#!forum/openrefine-dev) to see real use cases. 

Feel free to get in touch with us via [Gitter](https://gitter.im/OpenRefine/OpenRefine) or [the mailing list](https://groups.google.com/forum/#!forum/openrefine-dev) if you are interested. We also welcome spontaneous applications on projects of your own choosing!

We also offer [Outreachy projects](https://www.outreachy.org/apply/project-selection/#openrefine).

## Implement a SPARQL importer

* **Difficulty:** easy
* **Description:** SPARQL is a query language that can be used to get tabular data out of a triple store. It would be great if users could directly create a project from a SPARQL query to a given SPARQL endpoint without downloading the query results themselves, similarly to the existing SQL integration. There could be some interplay with reconciliation (the importer could create reconciled values directly).
* **Expected outcomes:** a new importer would be added, either in the core software or as an extension
* **Skills required/preferred:** this will require both backend (Java) and frontend (HTML/CSS/JS) work. Familiarity with RDF and SPARQL would also help.
* **Possible mentors:** [@wetneb](https://github.com/wetneb) ; [@ostephens](https://github.com/ostephens)
* **Relevant issues:** [#1212](https://github.com/OpenRefine/OpenRefine/issues/1212) and [some Documentation links discussion](https://groups.google.com/forum/?utm_medium=email&utm_source=footer#!msg/openrefine/lz7ac9ONro0/wFvg8WNoCAAJ) 

## OAuth support for Wikidata extension

* **Difficulty:** medium
* **Description:** the Wikidata extension currently uses password-based authentication to upload edits to Wikidata. We would like to support OAuth as well, which would make it easier to host OpenRefine instances online for multiple users to share. This will require improving the underlying library: [Wikidata-Toolkit](https://github.com/Wikidata/Wikidata-Toolkit/). It could also be interesting to work towards making the Wikidata extension configurable to work against other Wikibase instances.
* **Expected outcomes:** OpenRefine can be configured to use OAuth for authentication with Wikidata (password-based login is still the default)
* **Skills required/preferred:** primarily backend-side, so familiarity with Java is useful. Some understanding of OAuth would help but can be learnt on the fly too.
* **Possible mentors:** [@wetneb](https://github.com/wetneb)
* **Relevant issues:** [#1612](https://github.com/OpenRefine/OpenRefine/issues/1612), [Wikidata-Toolkit#268](https://github.com/Wikidata/Wikidata-Toolkit/issues/268), [#1640](https://github.com/OpenRefine/OpenRefine/issues/1640)

## Replace row pagination by infinite scrolling

* **Difficulty:** hard
* **Description:** When working on projects users currently view their table by pages of 5, 10, 25 or 50 rows. We would like to replace this system by a simple scrollable view, which would load the rows in the view dynamically. Various JavaScript libraries exist for this and could be reused. This is a highly requested feature that would greatly improve the tool.
* **Expected outcomes:** no more pagination system, the grid can be scrolled through naturally
* **Skills required/preferred:** frontend side (no changes to the Java backend should be required)
* **Possible mentors:** [@wetneb](https://github.com/wetneb) ; [@ostephens](https://github.com/ostephens)
* **Relevant issues:** [#1027](https://github.com/OpenRefine/OpenRefine/issues/1027), and many issues about the current pagination system: [#33](https://github.com/OpenRefine/OpenRefine/issues/33), [#570](https://github.com/OpenRefine/OpenRefine/issues/570), [#572](https://github.com/OpenRefine/OpenRefine/issues/572), [#1134](https://github.com/OpenRefine/OpenRefine/issues/1134)

## Reconciliation server within OpenRefine

* **Difficulty:** hard
* **Description:** OpenRefine could expose reconciliation services for the data stored in its own projects. This would make it possible to reconcile data from one project to another, providing a sort of "fuzzy join" between two projects. 
This requires implementing the [reconciliation API](https://reconciliation-api.github.io/specs/latest/) as a server in the backend. Such an implementation would be useful even if not all of the features of the reconciliation API are implemented initially.
* **Expected outcomes:** the OpenRefine backend can expose reconciliation services for projects in its workspace
* **Skills required/preferred:** primarily backend-side, so familiarity with Java is important.
* **Possible mentors:** [@wetneb](https://github.com/wetneb) [@tfmorris](https://github.com/tfmorris)
* **Relevant issues:** [#2003](https://github.com/OpenRefine/OpenRefine/issues/2003), [#941](https://github.com/OpenRefine/OpenRefine/issues/941), [#176](https://github.com/OpenRefine/OpenRefine/issues/176)

## Template

* **Difficulty:** 
* **Description:**
* **Expected outcomes:** 
* **Skills required/preferred:**
* **Possible mentors:** 
* **Relevant issues:** 
