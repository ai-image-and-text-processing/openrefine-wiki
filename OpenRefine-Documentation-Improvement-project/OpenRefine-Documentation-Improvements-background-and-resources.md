This document outlines the background to a project to improve the documentation available for OpenRefine at https://github.com/openrefine/openrefine/wiki

Ensuring OpenRefine has high quality documentation that is up to date and easy to use is an ongoing task that will need continued effort from the community. However, there are a number of issues with the current documentation that requires some concerted and organised effort to fix. The overall work to improve the documentation may require multiple phases, but the first and current phase aims to:

- Improve navigation within the documentation
- Ensure users and developers can easily find relevant documentation

The tasks identified to achieve this first phase are:

- Restructure the wiki to improve navigation
- Remove or update outdated documentation
- Move from Creole to MarkDown as the markup standard for the wiki

In the current phase the documentation will remain in a Github Wiki.
In the current phase there is no intention to extend the documentation substantially, although this may be required in a later phase.

## Contributing to the OpenRefine Documentation Improvements project

To contribute to the project you will need:

- Some familiarity with OpenRefine, but you don’t need to be an expert
- A Github account, but you don’t need to know how to use Git

If you would like to help with this project:
- Look at, and comment on, the [navigation and structure document](https://docs.google.com/a/ostephens.com/document/d/14OUv-4RbaCEV8IuKYCMd-8_MPKB78IE36ZNT1LXpBTQ/edit?usp=sharing)
- Look at the [workflow document](https://github.com/OpenRefine/OpenRefine/wiki/OpenRefine-Documentation-Improvements-workflow) and get started!

## Resources

The following resources may be helpful when working on the project

### How to edit Github Wiki pages

- [Editing wiki pages via the online interface](https://help.github.com/articles/editing-wiki-pages-via-the-online-interface/)
- [Adding and editing wiki pages locally](https://help.github.com/articles/adding-and-editing-wiki-pages-locally/)

### Guidance on writing documentation

- [What to Write by Jacob Kaplan-Moss](https://jacobian.org/writing/what-to-write/)

### Good practice examples of Github wiki documentation

- [Snowplow](https://github.com/snowplow/snowplow/wiki) - note how different sections have different navigation sidebars on the right-hand side
- [mcMMO](https://github.com/mcMMO-Dev/mcMMO/wiki)
- [d3](https://github.com/d3/d3/wiki) - especially the right-hand sidebar for navigation