## How to install extensions

Extensions can be installed in two locations. Either in the OpenRefine "workspace" directory (where all the OpenRefine project files are stored) or in the directory in which you have installed OpenRefine itself. In both cases extensions are installed by copying files into a directory called "extensions".

Extensions installed in the "workspace" directory are available to any version or install of OpenRefine you run which uses that workspace, whereas extensions installed in the OpenRefine directory are only available to that particular version/installation of OpenRefine.

To find the correct directory either:
* Find the OpenRefine folder on your computer, and locate the `extensions` folder/directory (usually this will be similar to `/OpenRefine/webapp/extensions`). If the `extensions` directory doesn't already exist, you can create it.

OR

* In OpenRefine's front page (http://127.0.0.1:3333/ ), click `Open Project` and then click the `Browse workspace directory` link at the bottom of the project list. This should open up the workspace directory in your system's file browser. In the workspace directory, create a sub-directory called "extensions" if there isn't one already.
  * For Linux, by default the workspace directory will be at `~/.local/share/openrefine/`
  * For Mac, by default the workspace directory will be at `~/Library/Application Support/OpenRefine`

To install an extension:

* Download the extension (usually as a Zip file)
* Unzip the extension into the `extensions` directory (this should create a sub-directory within the `extensions` directory which holds all of the files for the extension)
* Start (or restart) OpenRefine

**Note**: Some extensions are more _visible_ than others upon installation. Read the extensions documentation to see how to use it and how it appears in the OpenRefine interface (e.g. if the extension is supposed to appear in the upper right of your working window next to the "Extensions:" label, or if it works through the use of drop-downs or added functions within GREL).

**Note**: Not all extensions are compatible with all versions of OpenRefine. Check the extension documentation and ask the developers of the extension or the OpenRefine community for information about compatibility.


 