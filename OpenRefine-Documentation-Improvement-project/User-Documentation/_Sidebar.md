[**HOME**](Home) > [**Users**](Documentation-For-Users)

* [Menu under construction](new-Home)
* Installation and Configuration
    * [Installation Instructions](Installation-Instructions)
    * [Installing Extensions](Installing-Extensions)
    * [General FAQ](FAQ)
    * [FAQ Allocate more memory](FAQ-Allocate-More-Memory)
    * [FAQ Where is data stored](FAQ-Where-Is-Data-Stored)
* Using OpenRefine
* [Recipes](Recipes)
* Getting Help
* OpenRefine Reference
    * OpenRefine Variables
    * GREL Reference
    * OpenRefine Functionality Reference
* Other Resources
* [Contributing](https://github.com/OpenRefine/OpenRefine/blob/master/CONTRIBUTING.md)