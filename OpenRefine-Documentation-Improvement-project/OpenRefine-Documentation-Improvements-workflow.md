There are two stages to the workflow currently. These are discrete and can be carried out separately by different people.

## Stage 1: Review a wiki page

- Open the [tracker spreadsheet](https://docs.google.com/a/ostephens.com/spreadsheets/d/1VpU0g9jhyzqOV_6_bybozNH28syfbfTnUMTSBXfE9Ec/edit?usp=sharing) and pick a page to work on (that hasn’t already been reviewed)
- Add your initials against the row in Column D “Initials of Reviewer”
- Review the page and decide if:
  - It contains useful information that is up to date (“Leave”)
  - It contains useful information that needs updating (“Update”)
  - It does not contain useful information (“Remove”)
- Update the “Review Decision” in Column E
- If the Review Decision is “Remove” add your initials to Column H “Initials of Actioner” and update the date in Column I “Review actioned (date)”
- If the decision is to Leave or Update, indicate where the page will live in the new navigation in Column F “Location in new navigation” (see the navigation and structure document to understand the new structure of the documentation) 
- Feel free to add notes in Column G “Review notes” indicating anything specific that may need attention when the page is updated

## Stage 2: Update a wiki page

- Open the [tracker spreadsheet](https://docs.google.com/a/ostephens.com/spreadsheets/d/1VpU0g9jhyzqOV_6_bybozNH28syfbfTnUMTSBXfE9Ec/edit?usp=sharing) and pick a page which has already been reviewed i.e. has values in Column E “Review Decision” and Column F “Location in new navigation”, but has not yet been actioned 
- Add your initials against the row in Column H “Initials of Actioner”
- Create a new version of the page using Markdown syntax. The page should be added under the folder /Documentation-review-2017 in the appropriate place in the folder hierarchy, based on the value in Column F  “Location in new navigation”
- If the Column E “Review Decision” is Update, review the content of the page and update it to make sure it is current and relevant
- Once you have completed any revisions to the new version of the page, update the date in Column I “Review actioned (date)”, and add the URL of the new page in Column J “URL of updated page (if relevant)”
- Feel free to add notes in Column J “Action notes” if necessary