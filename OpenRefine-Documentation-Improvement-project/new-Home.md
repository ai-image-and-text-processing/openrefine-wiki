_This page is a work in progress as part of [a project to improve the documentation for OpenRefine](https://github.com/OpenRefine/OpenRefine/wiki/OpenRefine-Documentation-Improvements-background-and-resources)_

Welcome to the **OpenRefine** - a powerful tool for working with messy data: cleaning it; transforming it from one format into another; and extending it with web services and external data. 

This wiki provides the official documentation for both **users** and **developers** of OpenRefine. For a more general overview of OpenRefine and links to key resources, visit the [OpenRefine website](http://openrefine.org).

## Quick navigation

| User Guide                        | Developer Guide                 | Releases      |
|-----------------------------------|---------------------------------|---------------|
| [Installation and Configuration]()| [Architecture](https://github.com/OpenRefine/OpenRefine/wiki/Architecture)| [Download OpenRefine](https://github.com/OpenRefine/OpenRefine/releases)
| [Using OpenRefine]()              | [APIs]()                        |
| [Recipes - tips, tricks and ways of completing common tasks in OpenRefine](https://github.com/OpenRefine/OpenRefine/wiki/Recipes)                       | [Contributing]()                |
| [Getting help/support]()          |                                 |
| [OpenRefine Reference]()          |                                 |
| [Contributing]()                  |                                 |

## Questions or need help?

For general support or information queries, visit the [OpenRefine Mailing ](https://groups.google.com/group/openrefine/).
To submit a bug or make a feature requeset, [raise an issue on the OpenRefine Github](https://github.com/OpenRefine/OpenRefine/issues).

### Sponsorship

- OpenRefine is currently funded by [Google+Inc.+News+Lab](https://newslab.withgoogle.com/|)
- [Financials](https://docs.google.com/spreadsheets/d/17hldPOw8W\_TGM2DuKWbkFX-rexkQiE5esluZ8buadao/edit?usp=sharing) 

