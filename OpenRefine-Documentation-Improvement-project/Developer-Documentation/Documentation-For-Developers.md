## Architecture

* [Architecture](Architecture)
    * [Technology Stack](Technology-Stack)
    * [Server Side Architecture](Server-Side-Architecture)
    * [Client Side Architecture](Client-Side-Architecture)
    * [Importing Architecture](Importing-Architecture)
    * [Faceted Browsing Architecture](Faceted-Browsing-Architecture)

## APIs

* [OpenRefine API](OpenRefine-API)
* [Reconciliation API](Reconciliation-Service-API)
* [Suggest API](Suggest-API)
* [Data Extension API](Data-Extension-API)

## Contributing

### Get started

* [Contributing](https://github.com/OpenRefine/OpenRefine/blob/master/CONTRIBUTING.md)
    * [Development Roadmap](Roadmap)
    * [How to build, test and run OpenRefine from the source code](Building-OpenRefine-From-Source)
    * [Reporting issues](Issues)
    * [How to do an OpenRefine version release](Releasing-Version)
    * [Maintaining OpenRefine's Homebrew cask](Maintaining-OpenRefine's-Homebrew-Cask)

### Extensions

* Writing Extensions
    * [Write an Extension](Write-An-Extension)
    * [Extension Points](Extension-Points)
    * [Sample Extension](Sample-Extension)
    * [Migrating older extensions to Maven and Jackson](Migration-guide-for-extension-and-fork-maintainers)
* [Extensions already available](http://openrefine.org/download.html)

### i8n/Localisation

* [Translate the OpenRefine interface into other languages](Translate-OpenRefine)
