Currently supported languages include English, Spanish, Chinese, French, Hebrew, Italian and Japanese.

![Translation status](https://hosted.weblate.org/widgets/openrefine/-/287x66-grey.png)

You can help translate OpenRefine into your language by visiting [Weblate](https://hosted.weblate.org/engage/openrefine/?utm_source=widget) which provides a web based UI to edit and add translations and sends automatic pull requests back to our project.

Click to help translate --> [Weblate](https://hosted.weblate.org/engage/openrefine/?utm_source=widget)

## Front End 

The OpenRefine front end has been localized using [the recurser jquery-i18n plugin](https://github.com/recurser/jquery-i18n). The localized text is stored in a JSON dictionary on the server and retrieved with a new OpenRefine command.

[A piece of work has been started to replace this with the Wikidata jquery.i18n library](https://github.com/OpenRefine/OpenRefine/pull/1285)

### Adding a new string

There should be no hard-coded language strings in the HTML or JSON used for the front end.  If you need a new string, first check the existing strings to make sure there isn't an equivalent that you can reuse.  This cuts down on the amount of text which needs to be translated.

If there's no string you can reuse, allocate an available key in the appropriate translation dictionary and add the default string, e.g.

```json
...,
"dictionaryname":{
  "newkey":"new default string for this key",
},
...
```

and then set the text (or HTML) of your HTML element using i18n helper method. So given an HTML fragment like:
```html
<label id="new-element-id">[untranslated text would have appeared here before]</label>
```
we could set it's text using:
```
$('#new-html-element-id').text($.i18n._('dictionaryname')["newkey"]);
```
or, if you need to embed HTML tags:
```
$('#new-html-element-id').html($.i18n._('dictionaryname')["newkey"]);
```

### Adding a new language

The language dictionaries are stored in the `langs` subdirectory for the module e.g.

* https://github.com/OpenRefine/OpenRefine/tree/master/main/webapp/modules/core/langs for the main interface
* https://github.com/OpenRefine/OpenRefine/tree/master/extensions/gdata/module/langs for google spreadsheet connection
* https://github.com/OpenRefine/OpenRefine/tree/master/extensions/database/module/langs for database via JDBC
* https://github.com/OpenRefine/OpenRefine/tree/master/extensions/wikidata/module/langs for Wikidata

To add support for a new language, copy `translation-en.json` to `translation-<locale>.json` and have your translator translate all the value strings (ie right hand side).

#### main interface
 The translation is best done [with Weblate](https://hosted.weblate.org/engage/openrefine/?utm_source=widget). Files are periodically merged by the developper team.

 Run the latest (hopefully cloned from github) version and check whether translated words fit to the layout. Not all items can be translated word by word, especially into non-Ìndo-European languages.

 Several parts of the interface will remain in English even when you have checked all items. That's because they are integrated menus of extensions, not in the main interface. Don't worry.

#### extensions

Extensions can be translated via Weblate just like the core software.

The new extension for Wikidata contains lots of domain-specific concepts, with which you may not be familiar. The Wikidata may not have reconciliation service for your language. I recommend to check the glossary(https://www.wikidata.org/wiki/Wikidata:Glossary) to be consisitent.

By default, the system tries to load the language file corresponding to the currently in-use browser language. To override this setting a new menu item ("Language Settings") has been added at the index page.
To support a new language file, the developer should add a corresponding entry to the dropdown menu in this file: `/OpenRefine/main/webapp/modules/core/scripts/index/lang-settings-ui.html`. The entry should look like:
```javascript
<option value="<locale>">[Language Label]</option>
```

## Server / Back end

Currently no back end functions are translated, so things like error messages, undo history, etc may appear in English form.