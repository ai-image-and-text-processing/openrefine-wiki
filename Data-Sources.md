*List of useful places to get data*

# Data Sources

  - [The%20World%20Bank](http://data.worldbank.org/)
  - [The%20U.S.%20Government's%20Data](http://www.data.gov/)
  - [The%20U.K.%20Government's%20Data](http://data.gov.uk/
  - [European%20Community%20Financial%20Transparency%20System](http://ec.europa.eu/beneficiaries/fts/find\_en.htm\#)
  - [Gapminder](http://www.gapminder.org/data/)
  - [Infochimps](http://infochimps.org/)
  - [Many%20Eyes](http://manyeyes.alphaworks.ibm.com/manyeyes/datasets)
  - [Data%20360](http://www.data360.org/ds\_list.aspx)
  - [ScraperWiki](http://www.scraperwiki.com)
