*Listing of Reconcilable Data Sources*

With OpenRefine you can perform reconciliation against any web service
supporting the [[Reconciliation Service Api]].

The [Wikidata](https://www.wikidata.org) reconciliation service is
available by default in OpenRefine.

You can alternatively extend your data by
[[calling web services|Extending-Data]].

## Hosted services

These services can directly be added to OpenRefine using their URL by
clicking Reconcile -\> Add Standard Service. A
[more comprehensive list](https://reconciliation-api.github.io/testbench/)
is maintained on Wikidata.

### Wikidata

See our dedicated Wiki page:
[Reconciliation with Wikidata](https://github.com/OpenRefine/OpenRefine/wiki/Reconciliation)

### VIAF

The
[VIAF® (Virtual International Authority File)](http://viaf.org/)
combines multiple name authority files into a single OCLC-hosted name
authority service. The goal of the service is to lower the cost and
increase the utility of library authority files by matching and linking
widely-used authority files and making that information available on the
Web.

VIAF itself does not provide a reconciliation service but third-party
endpoints have been implemented:

  - http://refine.codefork.com/reconcile/viaf by Jeff Chiu, documented
    at http://refine.codefork.com/
  - http://iphylo.org/~rpage/phyloinformatics/services/reconciliation_viaf.php
    by Roderic D. M. Page, more details on
    [Reconciling author names using OpenRefine and VIAF](http://iphylo.blogspot.ca/2013/04/reconciling-author-names-using-open.html).

### VIVO Scientific Collaboration Platform

VIVO is a U.S. national interdisciplinary open source scientific
collaboration platform funded by the NIH with development led by
Cornell. Their reconciliation service allows reconciling against VIVO
entities (faculty members, journals, etc) in any VIVO installation.
[Extending Google Refine for VIVO](https://wiki.duraspace.org/display/VIVO/Extending+Google+Refine+for+VIVO)

Here are a few examples of such reconciliation endpoints:

  - Weill Cornell Medical College:
    http://vivo.med.cornell.edu/reconcile
  - Ben Gurion University of the Negev:
    https://scholars.bgu.ac.il/reconcile
  - Boise State University:
    https://boisestate.vivo.ctr-in.org/reconcile

### OpenCorporates

171 million corporate entities (as of Jul. 2019) available for
reconciliation through their service.

  - [Blog posts](http://blog.opencorporates.com/tag/google-refine/)
  - [Screencast](http://vimeo.com/17924204)
  - [Endpoint](https://opencorporates.com/reconcile)
  - [Documentation](https://api.opencorporates.com/documentation/Open-Refine-Reconciliation-API)

### Taxonomic Databases

Taxonomic databases (EOL,GBIF, NCBI,Global Names Index, uBio,
[WoRMS](http://www.marinespecies.org/aphia.php?p=webservice), as
documented
[here](http://iphylo.blogspot.com/2012/02/using-google-refine-and-taxonomic.html).

Taxonomic names from [IPNI](http://www.ipni.org/) via
[IPNI Names Reconciliation Service](http://data1.kew.org/reconciliation/).  
[Source code](https://github.com/RBGKew/Reconciliation-and-Matching-Framework)

### Organized Crime and Corruption Reporting Project

OCCRP provides a public reconciliation API endpoint which allows
reconciliation of data against a comprehensive list of sanctioned
persons and companies, politically exposed persons, and other persons of
journalistic interest. The service is intended as a first-level "check
for interesting entries" for government or private data.

  - [Official Website](https://data.occrp.org)
  - [Documentation](https://data.occrp.org/help/reconcile)
  - [Endpoint](https://data.occrp.org/api/freebase/reconcile)

### Nomisma

Nomisma provides data about numismatics:

  - [Blog%20post](https://numishare.blogspot.co.uk/2017/10/nomisma-launches-openrefine.html)
  - [Endpoint](http://nomisma.org/apis/reconcile)

### Ordnance Survey

Ordnance Survey is a national cartographic institution in the UK, which
provides reconciliation endpoints for various datasets.

  - [Documentation](http://data.ordnancesurvey.co.uk/docs/reconciliation)
  - [Tutorial](https://johngoodwin225.wordpress.com/2013/04/25/ordnance-survey-linked-data-and-the-reconciliation-api/)
  - [Example%20endpoint%20for%20UK%20postcodes](http://data.ordnancesurvey.co.uk/datasets/code-point-open/apis/reconciliation)

### FundRef

[The%20FundRef%20Reconciliation%20Service](https://www.crossref.org/labs/fundref-reconciliation-service/)
is designed to help publishers (or anybody) more easily clean-up their
funder data and map it to the FundRef Registry. It was built on FundRef
Metadata Search.

  - [Documentation](https://www.crossref.org/labs/fundref-reconciliation-service/)
  - [Endpoint](http://recon.labs.crossref.org/reconcile)

## Local services

You can run software alongside OpenRefine to provide other
reconciliation services, using data stored in various formats.

### Reconcile-csv

Reconcile-csv is a reconciliation service for OpenRefine running from a
CSV file. It uses fuzzy matching to match entries in one dataset to
entries in another dataset, helping to introduce unique IDs into the
system - so they can be used to join your data painlessly.

  - [Official%20Website](http://okfnlabs.org/reconcile-csv/)
  - [GitHub%20Project](https://github.com/okfn/reconcile-csv)

### SPARQL endpoints

The [RDF%20Extension](https://github.com/stkenny/rdf-extension) by DERI
at NUI Galway includes reconciliation against any SPARQL endpoint or RDF
dump file and publishing of the results in RDF. See the
[documentation](http://lab.linkeddata.deri.ie/2010/grefine-rdf-extension/)
for details.

For instance you can use this method to reconcile against the Library of
Congress Subject Headings (LCSH), as described by the
[Free%20Your%20Metadata%20group](http://freeyourmetadata.org/reconciliation/).

### conciliator

[conciliator](https://github.com/codeforkjeff/conciliator) is a Java
framework for creating OpenRefine reconciliation services. It currently
offers out of the box support for VIAF, ORCID, Open Library, and any
Apache Solr collection. Run your own service, or use the public server
at <http://refine.codefork.com>

### JournalTOCs

Use [JournalTOCs%20API](http://www.journaltocs.ac.uk/develop.php) to
create your own cool web applications that integrate content from freely
available journal TOCs. Most of JournalTOCs API calls are free and don't
require any registration process.

  - [GitHub%20Project](https://github.com/lawlesst/journaltocs-reconcile)

### FAST (Faceted Application of Subject Terminology)

FAST is derived from the Library of Congress Subject Headings (LCSH),
one of the library domain’s most widely-used subject terminology
schemas. The development of FAST has been a collaboration of OCLC
Research and the Library of Congress. Work on FAST began in late 1998.

  - [Official%20Website](http://www.oclc.org/research/activities/fast.html?urlm=159754)
  - [GitHub%20Project](https://github.com/lawlesst/fast-reconcile)

### Nomenklatura

Nomenklatura is a simple service that makes it easy to maintain a
canonical list of entities such as persons, companies or event streets
and to match messy input, such as their names against that canonical
list – for example, matching Acme Widgets, Acme Widgets Inc and Acme
Widgets Incorporated to the canonical “Acme Widgets”.

With Nomenklatura its a matters of minutes to set up your own set of
master data to match against and it provides a simple user interface and
API which you can then use do matching (the API is compatible with Open
Refine’s reconciliation function).

  - [Official%20Website](http://okfnlabs.org/projects/nomenklatura/index.html)
  - [GitHub%20Project](https://github.com/pudo/nomenklatura)

## Deprecated or defunct

### Freebase

The Freebase Reconciliation Service has been deprecated in June 2015 and
was shut down with the rest of Freebase later on.

<http://reconcile.freebaseapps.com/>

<https://developers.google.com/freebase/v1/reconciliation-overview?hl=en>

### Talis Kasabi

The Kasabi reconciliation services used to provide reconciliation
against any database published on the Kasabi platform.
[Former%20documentation](https://web.archive.org/web/20120525002556/http://kasabi.com/doc/api/reconciliation).
[They%20suggest%20some%20alternatives](http://blog.kasabi.com/2012/07/30/so-long-and-thanks-for-all-the-data/)

## Wish List

The following are data sources that could provide useful reconciling
within OpenRefine. If you would like to help with coding a reconciling
extension for any, please contact our mailing list. We would love to see
some of these happen\!

  - [Historical%20Newspapers](http://chroniclingamerica.loc.gov/about/api/)
    - Library of Congress' Chronicling America provides JSON, RDF, XML &
    Linked Data with an easy to use API.
  - [Chemical%20Identifier%20Resolver](http://cactus.nci.nih.gov/chemical/structure/documentation)
    - Over 96 million chemical structures hosted by NCI/NIH, provides
    names, conversions, & various formats even XML output.
  - [Global%20Research%20Identifier%20Database%20\(GRID\)](https://grid.ac/downloads)
    - Catalog of the world's research organisations, provides names,
    geographic data, id links to other databases, and
    inter-organizational relationships.
  - [OpenStreetMap](https://osm.org/) - collaborative map of the world.
    The reconciliation service could match against nodes, ways and
    relations. The data extension API could return coordinates and other
    attributes, so this would provide a way of doing geocoding. The
    extension could be built on top of existing services (such as
    [Nominatim)](https://nominatim.org/) and/or using its own index.
