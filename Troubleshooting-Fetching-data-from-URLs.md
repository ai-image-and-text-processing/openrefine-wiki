# Introduction

Through the "Add column by fetching URLs" function, OpenRefine supports
the ability to fetch HTML or data from web pages or services. When
OpenRefine attempts to fetch information from a web page or service, it
can fail in a variety of ways. The information on this page is meant to
help troubleshoot and fix problems encountered when using this function.

# Storing errors

To understand what might be going wrong, the first step is to make sure
that when you use the "Add column by fetching URLs" function change the
'On error' setting to 'store error'. By choosing this option you will
ensure that any errors OpenRefine encounters when trying to retrieve
data from a URL will be stored in the new column you are creating.

# Common errors

## "Received fatal alert: handshake\_failure"

This error can occur when you are trying to retrieve information over
HTTPS but the remote site is using an encryption not supported by the
Java virtual machine being used by OpenRefine.

You can check which encryption methods are supported by your
OpenRefine/Java installation by using a service such as "How's my SSL".
To do this add the URL "<https://www.howsmyssl.com/a/check>" to an
OpenRefine cell and use "Add column from URL". This will retrieve a
description of the SSL client being used by OpenRefine - this is likely
to be different to that being used by your web browser, as despite
running in your browser, when OpenRefine fetches data from a URL it uses
it's own code to fetch the data.

If you see this problem, you can try installing additional cipher suites
by installing the Java Cryptography Extension, which you can download
from
<http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html>.
This consists of two files (US\_export\_policy.jar and
local\_policy.jar).

Note on OpenRefine for Mac, these updated cipher suites need to be
dropped into the Java install within the OpenRefine application. For
example
"/Applications/OpenRefine.app/Contents/PlugIns/jdk1.8.0\_60.jdk/Contents/Home/jre/lib/security"
(the exact path may vary between local installations and depend on the
version of OpenRefine in use).

## "sun.security.validator.ValidatorException: PKIX path building failed"

This error can occur when you are trying to retrieve information over
HTTPS but the remote site is using a certificate which is not trusted by
your local Java installation. To trust the certificate you will need to
make sure that the certificate, or possibly (more likely) the root
certificate is trusted.

The list of trusted certificates is stored in an encrypted file called
'cacerts' in your local Java installation. This can be read and updated
by a tool called 'keytool'. Both
<http://javarevisited.blogspot.co.uk/2012/03/add-list-certficates-java-keystore.html>
and
<http://magicmonster.com/kb/prg/java/ssl/pkix_path_building_failed.html>
provide directions on how to add a security certificate to the list of
trusted certificates for a Java installation.

Note on OpenRefine for Mac, it is the 'cacerts' (certificates store)
file in the Java install within the OpenRefine application which needs
updating to trust the certificates. For example
"/Applications/OpenRefine.app/Contents/PlugIns/jdk1.8.0\_60.jdk/Contents/Home/jre/lib/security/cacerts"
(the exact path may vary between local installations and depend on the
version of OpenRefine in use).

## HTTP errors

When requesting a web page, or other file, over HTTP(S) the server that
responds to the request will always pass back an HTTP status code to
indicate whether the request has been successful or not (and if not,
what error was found).
[A%20list%20of%20HTTP%20status%20codes%20is%20available%20on%20Wikipedia](https://en.wikipedia.org/wiki/List\_of\_HTTP\_status\_codes).

If an HTTP error code is returned to OpenRefine, and you have indicated
to 'store errors' (as noted above), the HTTP status code and message
will be stored in the relevant cells.

From OpenRefine 3.0 onwards it is possible to set the following
[HTTP%20request%20headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers):
**
[User-Agent](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent)
**
[Accept](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept)
**
[Authorization](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization)**

To set these, click the 'Show' link next to "HTTP headers to be used
when fetching URLs"in the dialogue.

### "HTTP error 403 : Forbidden"

This error can be simply down to you not having access to the URL you
are trying to use to retrieve data. However, if you see this error when
you try to access a site using the OpenRefine "Add column by fetching
URLs" function, but when you try to access the same URL directly in your
browser it works without a problem, it is possible that the remote site
is blocking OpenRefine because it doesn't recognize it as a valid piece
of software/browser.

Changing the
[User-Agent](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent)
request header (click the 'Show' link next to "HTTP headers to be used
when fetching URLs"in the dialogue and edit the header to an appropriate
value) may help in these situations.

However, the User-Agent request header is only one of many ways of
limiting access to a resource, so changing this may have no effect.

If you believe you should have access to a site, but are receiving a
403: Forbidden error message, contacting the owner of the site you are
trying to access is probably the best course of action.

### "HTTP error 404 : Not Found"

This indicates that you have made a request for a URL that the remote
server does not recognize (i.e. doesn't exist).

### "HTTP error 500 : Internal Server Error"

This indicates that there has been an error on the remote server while
trying to fulfill your request.
