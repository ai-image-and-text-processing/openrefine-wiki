*Welcome to the OpenRefine wiki\!*

### Search this Wiki

by typing in the Search box in black bar above and then clicking on the
menu that pops up on the left and selecting "Wikis"(instead of the
default "Code").

### Here for the First Time?

  - Watch [[Screencasts]] to understand what's
    possible with OpenRefine
  - [[What People Say]]
  - [live on Twitter](https://twitter.com/search?q=OpenRefine)

### Get started:

  - [[Installation Instructions]]
  - [Preferences - we have an issue to document these better](https://github.com/OpenRefine/OpenRefine/issues/1677)
  - [[Documentation For Users]]
  - [[General Refine Expression Language\(GREL\) Reference|Documentation-For-Users\#reference]]
  - [[Common-Use-Case-Examples]]

### Get help:

  - [[FAQ]] Having TROUBLE? Check our Frequently Asked Questions
  - [[External Tutorials|External Resources]] in english, spanish, german, french and japanese
  - [[Recipes]] where folks might have solved a similar problem as yours
  - [Mailing List](https://groups.google.com/group/openrefine/)
  - [Chat on Gitter](https://gitter.im/OpenRefine/OpenRefine)
  - [[File bugs and feature requests here|issues]]

### Back for More?

  - [[Cool Extensions|Extensions]] for OpenRefine written by others
  - [[See what's new lately|Whats New]]
  - [[Related Software|Related Software]] that might fit your use case
    better than OpenRefine.
  - [[Understand how to upgrade|Back Up OpenRefine Data]] from
    Google Refine 2.0 to Google Refine 2.1

### Contributing

  - [[Contributing|Documentation For Developers\#contributing]]
  - [[Translate OpenRefine in your language|Translate OpenRefine]]

### If You Are a Developer, see

  - [[Documentation For Developers]]
  - [Developer MailingList](http://groups.google.com/group/openrefine-dev/)

### Sponsorship
  - OpenRefine is [currently funded Chan Zuckerberg Initiative (CZI)](http://openrefine.org/blog/2019/11/14/czi-eoss.html)
  - OpenRefine was funded in 2018 by [Google News Initiative](https://newsinitiative.withgoogle.com|)
