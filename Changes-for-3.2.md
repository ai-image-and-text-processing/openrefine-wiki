## New features
* New action to replace smart quotes to their ASCII equivalent ([#1676](https://github.com/OpenRefine/OpenRefine/issues/1676))
* New phonetic clustering methods are available: Beider-Morse ([#926](https://github.com/OpenRefine/OpenRefine/issues/926)) and Daitch-Mokotoff ([#927](https://github.com/OpenRefine/OpenRefine/issues/927))
* The "Uses values as identifiers" operation now accepts cells with RDF uris instead of just identifiers, using the `identifierSpace` declared by the reconciliation service ([#1953](https://github.com/OpenRefine/OpenRefine/issues/1953))
* Items suggested by auto-complete can now be clicked with the middle button, which opens their URL in a new tab ([#1934](https://github.com/OpenRefine/OpenRefine/issues/1934))
* Reconciliation previews are now shown when hovering the candidate (no click is needed). Clicking the candidate opens its page in a new tab. It is possible to disable this feature for matched cells by adding `cell-ui.previewMatchedCells=false` in the preferences. ([#1943](https://github.com/OpenRefine/OpenRefine/issues/1943))
* A "Select all/None" button was added when importing multi-sheet Excel ([#1974](https://github.com/OpenRefine/OpenRefine/issues/1974))
* New customized facets available in the "All" menu: facet by blank, non-blank values per column and non-blank records per column ([#1977](https://github.com/OpenRefine/OpenRefine/issues/1977))

## Wikidata
* Wikidata upload is twice as fast ([#2061](https://github.com/OpenRefine/OpenRefine/pull/2061))
* Labels and descriptions can be added without overwriting existing ones ([#2063](https://github.com/OpenRefine/OpenRefine/issues/2063))
* References in the Wikidata schema can be copied across statements and items ([#1912](https://github.com/OpenRefine/OpenRefine/issues/1912))
* The language codes supported by the Wikidata extension have been updated ([#1933](https://github.com/OpenRefine/OpenRefine/issues/1933))
* Reconciliation features are updated when creating new items with the Wikidata extension ([#1887](https://github.com/OpenRefine/OpenRefine/issues/1887))
* The date precisions supported in the Wikidata schema have been updated to match those supported by Wikidata ([#1944](https://github.com/OpenRefine/OpenRefine/issues/1944))
* A caching bug in the statement value rendering of the Wikidata extension has been fixed ([#2046](https://github.com/OpenRefine/OpenRefine/issues/2046))
* Adding multiple labels in the same language on an item in the Wikidata extension no longer fails ([#1917](https://github.com/OpenRefine/OpenRefine/issues/1917))
* Editing Wikidata as an anonymous user when login cookies are invalidated can no longer happen ([#2064](https://github.com/OpenRefine/OpenRefine/issues/2064))
* Incompatibility between schema export and QuickStatements export was resolved ([#2097](https://github.com/OpenRefine/OpenRefine/issues/2097))

## GREL
* The `cross` function outputs more informative error messages ([#1976](https://github.com/OpenRefine/OpenRefine/issues/1976))
* A bug in the `diff` GREL function was fixed ([#1998](https://github.com/OpenRefine/OpenRefine/issues/1998))
* The `find` GREL function now treats string litterals as such instead of as regular expressions ([#1833](https://github.com/OpenRefine/OpenRefine/issues/1833))

## Bug fixes
* The new behaviour of list facets, which required to add `.toString()` to the facet expression, has been temporarily reverted. It will be reintroduced in a non-breaking way as soon as possible. ([#1957](https://github.com/OpenRefine/OpenRefine/issues/1957))
* Reconciliation candidates are sorted by decreasing score even if the reconciliation service does not enforce that ([#1913](https://github.com/OpenRefine/OpenRefine/issues/1913))
* A data corruption bug introduced by reconciliation services which do not expose `identifierSpace` and `schemaSpace` was fixed ([#1936](https://github.com/OpenRefine/OpenRefine/issues/1936))
* Spaces in the path to OpenRefine's data directory are now allowed ([#1623](https://github.com/OpenRefine/OpenRefine/issues/1623))
* A bug (introduced in 3.2-beta) in cell value serialization which saved every value as a string was fixed ([#2008](https://github.com/OpenRefine/OpenRefine/issues/2008), [#1996](https://github.com/OpenRefine/OpenRefine/issues/1996))
* A bug (introduced in 3.2-beta) in Excel importing was fixed ([#1981](https://github.com/OpenRefine/OpenRefine/issues/1981), [#1984](https://github.com/OpenRefine/OpenRefine/issues/1984))
* A bug (introduced in 3.2-beta) in JSON history extraction was fixed ([#1990](https://github.com/OpenRefine/OpenRefine/issues/1990))
* A bug in column removal was fixed ([#2041](https://github.com/OpenRefine/OpenRefine/issues/2041))
* Exceptions when running OR from a non-existent workspace have been fixed ([#1989](https://github.com/OpenRefine/OpenRefine/issues/1989))
* A bug (introduced in 3.2-beta) when reading projects with custom metadata has been fixed ([#1994](https://github.com/OpenRefine/OpenRefine/issues/1994))
* A bug in operation JSON serialization was fixed ([#2068](https://github.com/OpenRefine/OpenRefine/issues/2068))

## Packaging and building
* Incompatibilities with Java 9 to 12 have been resolved ([#1890](https://github.com/OpenRefine/OpenRefine/issues/1890), [#2067](https://github.com/OpenRefine/OpenRefine/issues/2067), [#2069](https://github.com/OpenRefine/OpenRefine/issues/2069))
* The OpenRefine icon on the Windows executable was restored. ([#1995](https://github.com/OpenRefine/OpenRefine/issues/1995))
* A Maven issue when building the sources outside a Git repository was fixed ([#2047](https://github.com/OpenRefine/OpenRefine/issues/2047), [#2088](https://github.com/OpenRefine/OpenRefine/issues/2088))

## Vulnerabilities
* An XXE vulnerability with external DTDs was patched ([#1907](https://github.com/OpenRefine/OpenRefine/issues/1907))
* A vulnerability when importing compressed archives has been patched ([#1840](https://github.com/OpenRefine/OpenRefine/issues/1840))

## For developers

* OpenRefine now uses Jackson instead of org.json ([#1652](https://github.com/OpenRefine/OpenRefine/issues/1652)). Most extensions need to be migrated to Jackson as a consequence, see the [migration guide](https://github.com/OpenRefine/OpenRefine/wiki/Migration-guide-for-extension-and-fork-maintainers#migrating-from-orgjson-to-jackson) for that.
* Clustering is now extensible: it is possible to write OpenRefine extensions that define new clustering distances or encoders ([#1893](https://github.com/OpenRefine/OpenRefine/issues/1893)). A sample `phonetic` extension is provided, which implements the new phonetic methods mentioned above.
