*OpenRefine's Homebrew Cask*

## Introduction

[Homebrew](http://brew.sh] is a popular command-line package manager for
macOS. Installing Homebrew is accomplished by pasting the installation
command on the Homebrew website into a Terminal window. Once Homebrew is
installed, applications like OpenRefine can be installed via a simple
command:

## Usage

These instructions assume you've installed Homebrew. Instructions for
installing Homebrew are prominently displayed on the project's homepage,
<http://brew.sh>.

### Install OpenRefine

Install OpenRefine with this command:

``` 
  brew cask install openrefine
```

You should see output like this:

``` 
  ==> Downloading https://github.com/OpenRefine/OpenRefine/releases/download/2.7/openrefine-mac-2.7.dmg
  ######################################################################## 100.0%
  ==> Verifying checksum for Cask openrefine
  ==> Installing Cask openrefine
  ==> Moving App 'OpenRefine.app' to '/Applications/OpenRefine.app'.
  🍺  openrefine was successfully installed!
```

Behind the scenes, this command causes Homebrew to download the
OpenRefine installer (displaying progress as the download completes),
verify that the SHA-256 fingerprint of the download matches the value
stored in the Homebrew project, mount the disk image, copy the
OpenRefine.app application bundle into the Applications folder, unmount
the disk image, and save a copy of the installer and metadata about the
installation for future use.

If an existing OpenRefine.app is found in the Applications folder,
Homebrew will not overwrite it, so installing via Homebrew requires
either deleting or renaming previously installed copies.

### Uninstalling OpenRefine

To uninstall OpenRefine, paste this command into the Terminal:

``` 
  brew cask uninstall openrefine
```

You should see output like this:

``` 
  ==> Removing App '/Applications/OpenRefine.app'.
```

### Updating OpenRefine

To update to the latest version of OpenRefine, paste this command into
the Terminal:

``` 
  brew cask reinstall openrefine
```

You should see output like this:

``` 
  ==> Downloading https://github.com/OpenRefine/OpenRefine/releases/download/2.7/openrefine-mac-2.7.dmg
  ######################################################################## 100.0%
  ==> Verifying checksum for Cask openrefine
  ==> Removing App '/Applications/OpenRefine.app'.
  ==> Moving App 'OpenRefine.app' to '/Applications/OpenRefine.app'.
  🍺  openrefine was successfully installed!
```

If you had previously installed the "openrefine-dev" cask (containing a
release candidate) and you want to move to the stable release, you need
to first uninstall the old cask and then install the new one:

``` 
  brew cask uninstall openrefine-dev
  brew cask install openrefine
```
