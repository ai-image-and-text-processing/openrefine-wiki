*List of functions supported by the [[General Refine Expression Language]] (GREL)*

See also: [[GREL Functions]]

Function invocation in OpenRefine expression language has 2 forms:

  - `functionName(arg0, arg1, ...)`
  - `arg0.functionName(arg1, ...)`

The second is a shortcut to make expressions easier to read. It's only
syntactic sugar.

### Functions by Type

  - [GREL Boolean Functions](GREL%20Boolean%20Functions)
  - [GREL String Functions](GREL%20String%20Functions)
  - [ GREL Array Functions](GREL%20Array%20Functions)
  - [ GREL Math Functions](GREL%20Math%20Functions)
  - [ GREL Date Functions](GREL%20Date%20Functions)
  - [ GREL Other Functions](GREL%20Other%20Functions)
