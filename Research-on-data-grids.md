# Data grid libraries
## agGrid
MIT License  
recommended by Angular team to us...  
has most of what we need.  
it also has a grid API.  
Full width rows option might come in useful for record mode presentation later on?  
https://ag-grid.com/  

## Vue Good Table
MIT License  
Has support for most of the features we want.  
https://xaksis.github.io/vue-good-table/guide/configuration/#columns

## NGX-Datatable
MIT license  
an Angular 4+ data grid  
recommended by Angular team to us...
has most of what we need, some things that we envisioned for future OpenRefine versions like column grouping for records could be added in.  
http://swimlane.github.io/ngx-datatable/#  

## Pivottable.js
 - allow interactive record mode building ?  tons of ideas.
 - allow new working mode for OpenRefine , besides row & records, or in concert ?
 - more custom facets?  
https://pivottable.js.org/examples/

## React Virtualized
Looks to have fantastic React components for rendering large lists and tabular data.  
https://github.com/bvaughn/react-virtualized  
https://bvaughn.github.io/react-virtualized/#/components/Collection

## Datatables.net
https://datatables.net  
has locale based sort using collator  
https://datatables.net/blog/2017-02-28  
This doesn't have some core features we are also looking for.

## Tablecellselection
After researching, there just isn't enough here to help us with enough of the core datagrid features we want.  
https://github.com/likemusic/tablecellsselection  

## Jquery
Investigate and produce mapping file or suggested ways to move Jquery functions that now have equivalent Native functions.  
I don't entirely know what I'm talking about here.
Need to discuss with a UI person.

## Handsontable
**Incompatible License** (some wrappers also)  
PRO is needed for the features we want.  Open Source version doesn't have some of them.  We need to list the features we want and begin to work on a matrix table in Google Spreadsheets to see Pros/Cons of various Javascript Grid Libraries.  
https://github.com/handsontable/handsontable  