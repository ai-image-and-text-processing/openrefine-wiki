# New features
* Importing n-triples, ttl, and JSON-LD files is now possible ([#1758](https://github.com/OpenRefine/OpenRefine/issues/1758))
* The `smartSplit` function now supports any string, not just a single character. ([#1761](https://github.com/OpenRefine/OpenRefine/issues/1761))
* A new menu to search and replace was added ([#1742](https://github.com/OpenRefine/OpenRefine/issues/1742))
* A field to specify custom column names was added in the CSV/TSV importer 
* It is now possible to import and export a Wikidata schema in JSON ([#1776](https://github.com/OpenRefine/OpenRefine/issues/1776))
* Strings are now automatically trimmed in Wikidata schemas. The corresponding issues have been removed. ([#1781](https://github.com/OpenRefine/OpenRefine/issues/1781))
* Browser-based autocomplete has been enabled for Wikidata edit summaries. ([#1596](https://github.com/OpenRefine/OpenRefine/issues/1596))
* It is now possible to mark a column of identifiers as reconciled without calling the reconciliation service ([#1778](https://github.com/OpenRefine/OpenRefine/issues/1778))
* The GREL function `parseXml` was added ([#1818](https://github.com/OpenRefine/OpenRefine/issues/1818))
* The way text facets handle non-text values was changed. If you rely on this, make sure you add `.toString()` to the expressions used for text facets in your workflows. ([#1662](https://github.com/OpenRefine/OpenRefine/issues/1662))

# Bug fixes
* A bug in the Fill down / Blank down operations in records mode was fixed ([#1803](https://github.com/OpenRefine/OpenRefine/pull/1803))
* The layout of the encoding selection dialog was fixed ([#1812](https://github.com/OpenRefine/OpenRefine/issues/1812))
* A rendering issue for columns when switching back from the Wikidata tabs was fixed ([#1774](https://github.com/OpenRefine/OpenRefine/issues/1774))
* The blue triangle in the references section of Wikidata schemas is now clickable. ([#1780](https://github.com/OpenRefine/OpenRefine/issues/1780))
* The input field for properties in qualifiers and references is now focused when adding a new snak. ([#1780](https://github.com/OpenRefine/OpenRefine/issues/1777))
* Deprecated Wikidata constraints are now ignored in the issues. ([#1772](https://github.com/OpenRefine/OpenRefine/issues/1772))
* Integers are no longer rendered with a trailing ".0" in the Wikidata extension. ([#1772](https://github.com/OpenRefine/OpenRefine/issues/1775))
* Performing edits on Wikidata is more robust to HTTP errors returned by Wikidata. ([#1746](https://github.com/OpenRefine/OpenRefine/issues/1746))
* SQL importer now lets users add their own `LIMIT` and `OFFSET` to the query ([#1819](https://github.com/OpenRefine/OpenRefine/issues/1819))
* Quote escaping in SQL exporter was fixed ([#1802](https://github.com/OpenRefine/OpenRefine/issues/1802))
* The `cross` function accepts values of arbitrary types ([#1822](https://github.com/OpenRefine/OpenRefine/issues/1822))
* A bug with facets containing empty selections was fixed ([#1866](https://github.com/OpenRefine/OpenRefine/issues/1866))
* The `toDate` GREL function was fixed and improved ([#1759](https://github.com/OpenRefine/OpenRefine/issues/1759))

# For developers

* OpenRefine is now built with Maven instead of Ant ([#71](https://github.com/OpenRefine/OpenRefine/issues/71))
* OpenRefine now uses the Wikimedia i18n jQuery plugin ([#1279](https://github.com/OpenRefine/OpenRefine/issues/1279))
