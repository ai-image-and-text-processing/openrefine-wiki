# Step 1 - Download OpenJDK

1. Download OpenJDK from here: https://jdk.java.net/13/
2. On the jdk site, click on the blue links for either **tar.gz** or **ZIP** packages for your particular OS
3. Extract the **tar.gz** or **ZIP** file to a folder of your choice.
4. Proceed to **Step 2** for your particular OS.

# Step 2 - Configure the JAVA_HOME environment variable

## Windows 10
1. On Windows 10 Search bar input, type "env" and hit the ENTER key! (this will open the System Properties dialog)
![Windows 10 Search bar](https://user-images.githubusercontent.com/986438/69449357-5dadb080-0d20-11ea-9de9-541fe5a69850.png)
2. Then click Environment Variables... button at the bottom.
3. The Environment Variables dialog now appears.
4. Create a **JAVA_HOME** env variable and set the Value to the directory path of your JRE/JDK.

Example Screenshot from Windows 10:
 
![Windows 10 Environment Variables](https://user-images.githubusercontent.com/986438/69377067-1ddbc000-0c71-11ea-8e1a-86cb88195a7f.png)


## Mac OS
First, you can find where you java is on your computer with this command:<br/>
 `which java`

You need to make sure that the variable `JAVA_HOME` is set and you can do that with <br/>
`$JAVA_HOME/bin/java --version`

To set the environment variable, use the next command for Java 13.x : <br/> 
 `export JAVA_HOME="$(/usr/libexec/java_home -v 13)"`

or for the current Java version of your MacOS:<br/>
 `export JAVA_HOME="$(/usr/libexec/java_home)"`

You should be set!

## Linux
Reference:
https://medium.com/@charinin/setting-java-home-environment-variable-in-ubuntu-e355c80e5b6c

