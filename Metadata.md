
## How to add user defined metadata?
To add user defined metadata, go to "[preferences](http://127.0.0.1:3333/preferences)" at the bottom of the left menu bar. Click "add"(or "edit" if already there", then input the following:
* Key: userMetadata
* Value: Value is a json array in which have 2 elements with below format:

` {"name":"YOUR METADATA NAME","display":true[false]}`
"display" key is a Boolean type flag of whether the column will be displayed on the project metadata page or not. Multiple user defined metadata can be defined with or without display.

Example: 

`[{"name":"client name","display":true},{"name":"progress","display":false},{"name":"test1","display":true},{"name":"test2","display":true},{"name":"test444444","display":true}]`

After setting the preferences, you can reload the project page and then you can start to set the user defined metadata from the "About" link located at the right the project.

