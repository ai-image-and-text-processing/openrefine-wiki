Glossary of Terms

Blank Down: Blank down means to replace the duplicate entries in subsequent rows with blanks. This forms gaps between rows of identical values in a column. I.E. "Blank down to replace the IDs of
duplicate rows by blanks (every original row will keep its ID, only subsequent ones
will lose them). " This is a common step in converting from Rows to Records.

Records & Rows: http://kb.refinepro.com/2012/03/difference-between-record-and-row.html

General Refine Expression Language (GREL): previously known as "Google Refine Expression Language", this is the language used to write data transformations in OpenRefine
Google Refine Expression Language (GREL): Obsolete name for General Refine Expression Language
