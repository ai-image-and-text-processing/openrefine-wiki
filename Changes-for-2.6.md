*Changes for OpenRefine v2.6 release*

-----

UNDER CONSTRUCTION - this list isn't complete

-----

# New Features

  - New branding\! - we're now OpenRefine instead of Google Refine
  - Internationalization - the UI is available in Italian as well as
    English
  - Freebase Suggest 4.2+ & new Freebase APIs including a new Freebase
    reconciliation service

# Enhancements

  - Better handling of various project creation cases:
      - Zip files and archives containing sub-directories
        ([issue%20#144)](https://github.com/OpenRefine/OpenRefine/issues/144)
      - Better defaults & more options to allow importing data without
        any changes
        ([issue%20#)](https://github.com/OpenRefine/OpenRefine/issues/)
      - Shorter names for columns on JSON & XML import
        ([issue%20#524)](https://github.com/OpenRefine/OpenRefine/issues/524)
      - Better heap memory usage reporting (issue
        [#604))](https://github.com/OpenRefine/OpenRefine/issues/](#467),[https://github.com/OpenRefine/OpenRefine/issues/604)
  - Better support for internationalization in clustering
  - ...

# Bug Fixes

  - ...

## Miscellaneous

  - Issue 000:
    ([issue%20#)](https://github.com/OpenRefine/OpenRefine/issues/)

[Full%20list%20of%20Issues](https://github.com/OpenRefine/OpenRefine/issues?milestone=1).
