# Steps to obtain OAuth credentials:

### For the development version of OpenRefine only.

To obtain data from [Google Drive](https://www.google.com/drive/) or [Google Sheets](https://www.google.com/sheets/about/) for OpenRefine requires OAuth credentials. For obtaining the OAuth credentials follow these steps:

* Go to the [Google API Console](https://console.developers.google.com/)
* Click on **Create Project**.
* Pick a project name such as **My OpenRefine Dev Project**.
* Fill the required fields.
* Hit **Create** for creating the project. 
* Pick **Enable APIs and Services**.

**Enable the following services:**
* Google Sheets API
* Google Drive API

**OAuth consent screen pane:**
- Go to the **OAuth consent screen pane**.
- As user type, pick **External**. 
- Fill the form for the OAuth credentials. 
- For the OAuth scopes, pick ../auth/drive/ which will give you write access to your Google Drive.
- For the domain (and other URLs), you can pick anything (since the localhost will be allowed anyway). 
- Hit **Save** to save the consent screen.

**Credentials pane:**
- Go to the **Credentials pane**.
- Create new OAuth credentials, adding as allowed origin `http://127.0.0.1:3333`, and as allowed callback URL: `http://127.0.0.1:3333/extension/gdata/authorized`
- Copy your **client id** and **client secret key**.
- Save them in a file outside your git clone of OpenRefine.
- Add your client id and secret key in the GoogleAPIExtension class.