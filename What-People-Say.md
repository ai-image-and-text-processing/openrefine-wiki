*Curated list of quotes, references, uses and extensions about
OpenRefine (previously called Google Refine & Freebase Gridworks) in the
wild*

# About Google Refine

### Tweets

"Damn\! Wish I had this 5 years ago\! RT @swiertz nice tools \! Format &
clean your data with Google Refine <http://goo.gl/UniR6> \#cleanup
\#tools"

[Xavier%20Bartholome](http://twitter.com/barthox),
[view%20tweet](http://twitter.com/barthox/status/4803982065147904)

"YIPEEEE\! Google Refine works with OAI-PMH XML out of the box. This is
going to make my life much easier."

[Richard%20Urban](http://twitter.com/Musebrarian),
[view%20tweet](http://twitter.com/Musebrarian/status/4723491286810624)

"It’s kind of ridiculous how exciting I find this:
<https://code.google.com/p/google-refine/>"

[kb](http://twitter.com/kb),
[view%20tweet](http://twitter.com/kb/statuses/5177336995512320)

"Google Refine is like a dream come true. \#grefine
<http://sunlightfoundation.com/blog/2011/01/06/tools-for-transparency-google-refine/>
"

[Liam%20Arbetman](http://twitter.com/liamaa),
[tweet](http://twitter.com/liamaa/status/23103260483129344)

"I rarely feel the desire to kiss a corporation on the mouth, but Google
Refine is making me come close <http://goo.gl/8pvKB> \#datageek"

[litcritter](http://twitter.com/litcritter),
[view%20tweet](http://twitter.com/litcritter/statuses/5296061488828416)

"For those who haven't looked yet, Google Refine / Gridworks 2.0 blows
the doors off. Its the single best tool I'm using."

[Christopher%20Groskopf](http://twitter.com/onyxfish),
[view%20tweet](http://twitter.com/onyxfish/statuses/4599207788806144)

"I'm sold on \#Google \#Refine used it most of the day with "messy" data
and managed to clean nearly all of it."

[Learon%20Dalby](http://twitter.com/LearonDalby),
[view%20tweet](http://twitter.com/LearonDalby/statuses/4303660389502976)

"Today google \#refine saved my afternoon. Every \#data \#hacker should
try it"

[roolio](http://twitter.com/roolio),
[view%20tweet](http://twitter.com/roolio/statuses/4187886568083456)

"Google refine is awesome. Never before have I been home this early."

[Salesient](http://twitter.com/Salesient),
[view%20tweet](http://twitter.com/Salesient/statuses/4242219707928576)

"I am having far, far too much fun with Google Refine. This tool is
ridiculously powerful, even without Freebase integration."

[Jerry%20Towler](http://twitter.com/jatowler),
[view%20tweet](http://twitter.com/jatowler/statuses/5542139572264960)

"Not only will it clean your data, Google Refine will slice, dice and
put bows on your hairdo\! <http://bit.ly/cPGn1E> Rocks data
exploration."

[Mario%20Talavera](http://twitter.com/Mayin),
[view%20tweet](http://twitter.com/mayin/status/5386710112276481)

"Google Refine: Making interns unneccesary since 2010."

[Mark%20Labedz](http://twitter.com/marklabedz),
[view%20tweet](http://twitter.com/marklabedz/statuses/9633943271772160)

"i'm completely in love with Google Refine. fo' reals."

[Nater%20Kane](http://twitter.com/naterkane),
[view%20tweet](http://twitter.com/naterkane/statuses/9981143436763136)

"Using \#Google \#Refine makes me happy. Even for the easy stuff."

[Learon%20Dalby](http://twitter.com/LearonDalby),
[view%20tweet](http://twitter.com/LearonDalby/statuses/10370150263947266)

"Google Refine: love at first click"

[Loran%20Cook](http://twitter.com/loranstefani),
[view%20tweet](http://twitter.com/loranstefani/statuses/18051195771817984)

"Um, this intro to Google Refine makes the data-lover in me uber-horny.
<http://bit.ly/8XN0xU> \[humps monitor\]"

[Kim%20M.](http://twitter.com/kimmieoftroy),
[view%20tweet](http://twitter.com/kimmieoftroy/status/22388155407667201)

"Google Refine 2.0 makes this reporter so happy he could hug it. Except
that he has a rule: no hugging Google products."

[Jeff%20Severns%20Guntzel](http://twitter.com/jsguntzel),
[view%20tweet](http://twitter.com/jsguntzel/statuses/22658537058467840)

"Google Refine is gonna change my life"

[Tracy%20Tran](http://twitter.com/tracyctran),
[view%20tweet](http://twitter.com/tracyctran/status/27422734816382977)

Oh man\! Google Refine is making our lives so much easier, shaving hours
off analysing keyword data: <http://t.co/WQotutC>

[woofonline](http://twitter.com/woofonline),
[view%20tweet](http://twitter.com/woofonline/status/28130932099645440)

Google Refine, I freakin' love you. \#Badass \#NerdLove \#fangirl
\#NewBoyfriendDay

[Rhiannon%20Coppin](http://twitter.com/rhiannoncoppin),
[view%20tweet](http://twitter.com/rhiannoncoppin/status/31149069439078400)

If only more people knew how amazingly powerful Google Refine was, the
world would be a different place.

[irwin](http://twitter.com/irwin),
[view%20tweet](http://twitter.com/irwin/status/34989881616891904)

Mind officially blown by how great Google Refine looks for cleaning
messy data. Thank you, @dfhuynh: <http://ow.ly/43YnO> \#nicar11

[Tom%20Meagher](http://twitter.com/ultracasual),
[view%20tweet](http://twitter.com/ultracasual/status/41543073511112704)

Dear Google \#Refine, thank you for existing and saving me from my Cut
and Paste nightmares. <http://code.google.com/p/google-refine/>

[David%20Monroe](http://twitter.com/Dorje),
[view%20tweet](http://twitter.com/Dorje/status/43364623860252672)

Google you have made my world 400% easier with google refine. Im your
friend.

[davvyk](http://twitter.com/davvyk),
[view%20tweet](http://twitter.com/davvyk/status/58545718415204352\#)

Not too many programs are a pleasure to work with to the level of Google
Refine

[Victor%20Castell](http://twitter.com/victorcoder),
[view%20tweet](http://twitter.com/victorcoder/status/71132170948845568)

Google Refine, in one minute, has saved me hours of work\!
<http://bit.ly/d3mIXG>

[Aaron%20Rubinstein](http://twitter.com/rubinsztajn),
[view%20tweet](http://twitter.com/rubinsztajn/status/74164857364156417)

@mattwaite tells of his despair over the hours spent cleaning data that
can now be cleaned in a few minutes in Google Refine \#IRE11 \#gannett

[Mark%20Schaver](http://twitter.com/markschaver),
[view%20tweet](http://twitter.com/markschaver/status/78837142964547584)

@mattwaite also says \#IRE2011 "It makes me want to drown in a bathtub
when I look at Google Refine... all the hours of my life I've lost."

[alicitabrennan](http://twitter.com/alicitabrennan),
[view%20tweet](http://twitter.com/alicitabrennan/status/78836868719984640)

Google Refine: An empiricist's new best friend. - If you’ve never seen
it before, Google Refine is a free... <http://tumblr.com/x8r34dba7z>

<http://twitter.com/Jaron_In_Theory>,
[view%20tweet](http://twitter.com/Jaron\_In\_Theory/status/83582754364735488)

Finally played around with @google refine. In two clicks it did what
took me several steps in \#Access. \#prettyawesome <http://t.co/8Pj7ebE>

<http://twitter.com/lisachiuster>,
[view%20tweet](http://twitter.com/lisachiuster/status/83315910613540865)

Today my life was made much easier by Google Refine
(<http://t.co/gZm3T9X>). I love it.

[VanL](http://twitter.com/VanL),
[view%20tweet](http://twitter.com/VanL/status/103517015851020288)

Totally and utterly in love with Google Refine right now...

[Joanna%20Geary](http://twitter.com/timesjoanna),
[view%20tweet](http://twitter.com/timesjoanna/status/104260882208522240)

@timesjoanna google refine is the proverbials, one of the most useful
bit of data manipulation software i've used in, well, ever

[Andrew%20Walkingshaw](http://twitter.com/covert),
[view%20tweet](http://twitter.com/covert/status/104261036047216640)

The more I learn about Google Refine the more excited I get about using
it. \#Analyst4Life

[Kim%20Z%20Dale](http://twitter.com/observacious),
[view%20tweet](http://twitter.com/observacious/status/108264180527792128)

@edwardog @leslieyoung GRefine = godsend ... :)

[buzzdata](http://twitter.com/buzzdata),
[view%20tweet](http://twitter.com/buzzdata/status/113634815487655936)

Cleaning up data using Google Refine. It's a gift sent from heaven

[Ben%20Poston](http://twitter.com/bposton),
[view%20tweet](http://twitter.com/bposton/status/122056208709459970)

I think I've fallen in love with google refine \#infovis

[Jeremy%20Field](http://twitter.com/jeremy\_field),
[view%20tweet](http://twitter.com/jeremy\_field/status/126995834159116288)

Google Refine might be the greatest thing on the planet.

[Greg%20Reda](http://twitter.com/gjreda),
[view%20tweet](http://twitter.com/gjreda/status/144226939723448320)

Crushin' on a project. The object of my affect doesn't even have to be
human. Google Refine is so sharp\!

[Kasia%20Hayden](http://twitter.com/haydenkasia),
[view%20tweet](http://twitter.com/haydenkasia/status/144317138042228736)

Google Refine is my friend today.

[mrgunn](http://twitter.com/mrgunn),
[view%20tweet](http://twitter.com/mrgunn/status/144855308311859200)

The award for the biggest timesaver goes to Google Refine and its data
reconciliation \#epic

\[<https://twitter.com/electricbum> Hannes Ebner\],
[view%20tweet](https://twitter.com/electricbum/status/164789954059309056)

10 minutes into the Google Refine session and I'm in love. \#dirtydata
\#Nicar12

\[<https://twitter.com/amyjo_brown> AmyJo Brown\],
[view%20tweet](https://twitter.com/amyjo\_brown/status/173138136489136129)

I may be late but this is quite possibly the coolest thing I've seen
recently since the other side of the pillow:
<http://code.google.com/p/google-refine/>

[Jeff%20Rush](http://twitter.com/jeffrush),
[view%20tweet](http://twitter.com/jeffrush/status/179296155711193089)

Google Refine is amazing - fast cluster heuristics intelligently clean
up your data. Best find of 2012 so far... \#analytics \#data

[Senan](http://twitter.com/NativeAnalytics),
[view%20tweet](http://twitter.com/NativeAnalytics/status/179326447620591616)

I seriously love Google Refine. Dead simple and very useful. You don't
want a Swiss army knife. Just a simple, simple hammer.

[Eric%20Feng](http://twitter.com/ehfeng),
[view%20tweet](http://twitter.com/ehfeng/status/179496038548250624)

I love Google Refine.

[Michael%20Grimes](http://twitter.com/citizensheep),
[view%20tweet](http://twitter.com/citizensheep/status/179554740521275393)

Where does Google come up with this stuff? Amazing data manipulation
tool\!

[\(eaɹsmack\)](http://twitter.com/earsmack),
[view%20tweet](http://twitter.com/earsmack/status/187921361480593410)

Waking up and trying to organize data with Google Refine is my version
of Saturday morning cartoons.

[Troy%20Griggs](http://twitter.com/TroyEricG),
[view%20tweet](http://twitter.com/TroyEricG/status/188640514906656769)

Google Refine.. absolutely beautiful. @Reggieonthereg @IUSamParsons rock

[John%20Sibo](http://twitter.com/iRideSiboats),
[view%20tweet](http://twitter.com/iRideSiboats/status/189153182620983296)

@cspenn I LOVE Google Refine -- super fast to clean up tons of data\! cc
@johnjwall @iPullRank

[Slavik%20Volinsky](http://twitter.com/svolinsky),
[view%20tweet](http://twitter.com/svolinsky/status/200678775975981056)

not sure how i ever did my job without google refine, now that i've
figured it out

[kevin%20clair](http://twitter.com/kevinclair),
[view%20tweet](http://twitter.com/kevinclair/status/200581717738795008)

Goble says google refine - the lords' work\! ‪\#jcdl2012‬

[maureen%20henninger](http://twitter.com/maureenh1),
[view%20tweet](http://twitter.com/maureenh1/status/212535889967067136)

Google Refine, take my pain away.

\[<https://twitter.com/MrDys> Sean Hannan\],
[view%20tweet](https://twitter.com/mrdys/status/220246953596948481)

Google Refine: Where the hell have you been all my life? Excel for Big
Data/Regex geeks

\[\[<https://twitter.com/geoffreymccaleb> Geoffrey McCaleb\],
[view%20tweet](https://twitter.com/geoffreymccaleb/status/220158210361929730)

Google Refine is (an outstanding) power tool for working with messy
data. <http://bit.ly/ak6gfC> Thank you, GR devs\! \#DataScience

[Harlan%20Harris](http://twitter.com/HarlanH),
[view%20tweet](http://twitter.com/harlanh/status/241259084421156864)

Just spent 10 hours playing with Google Refine like it was my new
favorite video game.

[Kim%20Witten](http://twitter.com/iamkimiam),
[view%20tweet](http://twitter.com/iamkimiam/status/240930667955904513)

Today is yet another day where Google Refine made my life better.

[Chris%20Weber](http://twitter.com/chris\_m\_weber),
[view%20tweet](http://twitter.com/chris\_m\_weber/status/253157884295274496)

Oh Google Refine, you compl33t me.

\[<https://twitter.com/doriantaylor> Dorian Taylor\],
[view%20tweet](https://twitter.com/doriantaylor/status/258771194520686592)

I would have sold my soul for this in 1999. <https://t.co/FKw6U7XZ>

<https://twitter.com/k808a Ken Andrade>,
[view%20tweet](https://twitter.com/k808a/status/299352748577677312)

Kind of in love with Google Refine right now.

<https://twitter.com/Margaret_Heller Margaret Heller>,
[view%20tweet](https://twitter.com/Margaret\_Heller/status/299607414754398209)

### Blogs

"Google Refine isn’t going to solve the problem of poor data
availability, but for those who manage to gain access to existing
records, it can be a powerful tool for transparency."

[Rebekah%20Heacock](http://jackfruity.com/), co-director of the
[Technology%20for%20Transparency%20Network](http://transparency.globalvoicesonline.org/)
and a Project Coordinator at Harvard’s
[Berkman%20Center%20for%20Internet%20and%20Society](http://cyber.law.harvard.edu/)
- [Sunlight%20Foundation](http://sunlightfoundation.com/blog/),
[Tools%20for%20transparency:%20Google%20Refine](http://sunlightfoundation.com/blog/2011/01/06/tools-for-transparency-google-refine/).

"Google Refine is an immensely powerful tool for dealing with "messy"
data, and it sports a myriad of advanced features for massaging and
analyzing complex data sets"

Dmitri Popov ([Linux%20Magazine)](http://www.linux-magazine.com/) -
[Use%20Google%20Refine%20to%20Massage%20Your%20Data](http://www.linux-magazine.com/Online/Blogs/Productivity-Sauce/Use-Google-Refine-to-Massage-Your-Data)

"For anyone who’s ever had to sort through messy data to try to turn up
a meaningful treatment, and who hasn’t, this tool is a godsend."

[Michael%20Lines](http://library.law.uvic.ca/services/contact-us/mrl),
[SLAW](http://www.slaw.ca/) -
[Google%20Refine%202.0](http://www.slaw.ca/2010/11/12/google-refine-2/)

"Google Refine 2.0 will serve an excellent back-end for data
visualization services. It has been well received by the Chicago Tribune
and open-government data communities. Along with Google Squared, Refine
2.0 can create a powerful research tool."

Chinmoy Kanjilal, Techie Buzz -
[Google%20Refine%202.0:%20Power%20Tools%20for%20Working%20With%20Data](http://techie-buzz.com/foss/google-refine-data-tool.html)

Google Refine (the program formerly known as Freebase Gridworks) is
described by its creators as a “power tool for working with messy data”
but could very well be advertised as “remedy for eye fatigue, migraines,
depression, and other symptoms of prolonged data-cleaning.”

Dan Nguyen, ProPublica -
[Chapter%201.%20Using%20Google%20Refine%20to%20Clean%20Messy%20Data](http://www.propublica.org/nerds/item/using-google-refine-for-data-cleaning)

The functionality offered by Google Refine is paramount and provides the
user with a simple interface with powerful data manipulation
capabilities. ... I solicited Ryan Boyles to develop a script to
sanitize the data which, in total, took about 45 minutes to get the data
into a proper format. Utilizing Google Refine I was able to replicate
the work of his script in about 15 minutes\!

Logan Lynn, The Data Revolution -
[Get%20Started%20With%20Google%20Refine%202.0](http://www.thedatarevolution.com/2011/01/14/get-started-with-google-refine-2-0/)

Google Refine for SEO: \[\["When I first saw Freebase Gridworks I was a
very happy man. Here was a tool that tackled one of the biggest problems
in data journalism: cleaning dirty data (and data is invariably dirty).
The tool made it easy to identify variations of a single term, and clean
them up, to link one set of data to another – and much more besides."

\[<http://ojb.journallocal.co.uk> Online Journalism
Blog|<http://www.blindfiveyearold.com/find-keyword-modifiers-with-google-refine>\]\]\]
post
[Data%20cleaning%20tool%20relaunches:%20Freebase%20Gridworks%20becomes%20Google%20Refine](http://ojb.journallocal.co.uk/2010/11/11/data-cleaning-tool-relaunches-freebase-gridworks-becomes-google-refine/)

Google Refine bills itself as a “power tool for working with messy
data,” and it does not disappoint. While not a turnkey solve-all for
data integrity, it makes this tedious task far less intimidating. ...
Dirty data is here to stay. But with Refine, at least it’s no longer the
daunting task it once was.

[Matt%20Wynn](http://www.poynter.org/author/matt-wynn/) -
[How%20journalists%20can%20use%20Google%20Refine%20to%20clean%20‘dirty’%20data%20sets](http://www.poynter.org/how-tos/digital-strategies/155975/how-journalists-can-use-google-refine-to-clean-dirty-data-sets/)

# About Freebase Gridworks 1.x

### Blog Posts

"Refine really shines when it is used to combine or transform data from
multiple sources, so it's no surprise that it has been popular for open
government and data journalism tasks. ... Best of all, Google Refine
keeps a running changelog that lets you review and revert changes to
your data -- so go ahead: play around."

[Julie%20Steele](http://radar.oreilly.com/julies/index.html) (O'Reilly
Radar) -
[Strata%20week:%20Keeping%20it%20clean](http://radar.oreilly.com/2010/11/strata-week-keeping-it-clean.html)

"As the open data juggernaut picks up steam, a lot of folks are going to
discover what some of us have known all along. Much of the data that’s
lying around is a mess. That’s partly because nobody has ever really
looked at it. As a new wave of visualization tools arrives, there will
be more eyeballs on more data, and that’s a great thing. But we’ll also
need to be able to lay hands on the data and clean up the messes we can
begin to see. As we do, we’ll want to be using tools that do the kinds
of things shown in the Gridworks screencasts."

[Jon%20Udell) (Microsoft](http://en.wikipedia.org/wiki/Jon\_Udell) -
[Freebase%20Gridworks:%20A%20power%20tool%20for%20data%20scrubbers](http://blog.jonudell.net/2010/03/26/freebase-gridworks-a-power-tool-for-data-scrubbers/)

"... PowerPivot and Gridworks. Each, on its own, is an amazing tool. But
the combination makes my spidey sense tingle in a way I haven’t felt for
a long time."

[Jon%20Udell) (Microsoft](http://en.wikipedia.org/wiki/Jon\_Udell) -
[PowerPivot%20+%20Gridworks%20=%20Wow!](http://blog.jonudell.net/2010/04/19/powerpivot-gridworks-wow/)

"...the tool looks absolutely fantastic."

[Simon%20Willison](http://en.wikipedia.org/wiki/Simon\_Willison) -
[Preview:%20Freebase%20Gridworks](http://simonwillison.net/2010/Mar/27/gridworks/)

"This is a tremendous contribution to the community, especially given
that comparable commercial products are priced well out of range of
small businesses, independent developers, and newsrooms. While we can’t
tell you about any of the projects we are using it on just yet, we can
say that its changing the way we look at data on a daily basis. We
really can’t say enough about what a great application Gridworks is and
about its myriad uses for hacker journalists and data-nerds of all
stripes."

[Christopher%20Groskopf](http://www.linkedin.com/in/christophergroskopf)
(Chicago Tribune) -
[Data%20normalization%20par%20excellence–the%20gift%20of%20Freebase%20Gridworks](http://blog.apps.chicagotribune.com/2010/05/17/the-gift-of-freebase-gridworks/)

"If you’re concerned with building and maintaining collections of
semi-structured data, or building your own technology for this purpose,
I suggest you check out these state-of-the-art tools."

[Daniel%20Tunkelang) (Google](http://www.linkedin.com/in/dtunkelang) -
[Gridworks%20and%20Needlebase](http://thenoisychannel.com/2010/06/20/gridworks-and-needlebase/)

"Gridworks is a simply amazing tool for data cleansing, analysis and, as
we’ve seen, transformation. It’s set to become more so for our purposes
in the near future, as it comes to support the mapping of names for
things to URIs using configurable reconciliation services (which might
allow it to automatically map Government Department names to URIs, for
example), and the creation of RDF using a more intuitive and
user-friendly approach than the templates that I’ve illustrated here."

Jeni Tennison -
[Using%20Freebase%20Gridworks%20to%20Create%20Linked%20Data](http://www.jenitennison.com/blog/node/145)

### Tweets

"Gridworks is like crack for data junkies"

[Chris%20Amico](http://twitter.com/eyeseast)
([PBS%20NewsHour)](http://www.pbs.org/newshour/)
[view](http://twitter.com/eyeseast/status/14451895341)

"I think @thejefflarson is going to name a dog after Gridworks (speaking
of his colleague Jeff Larson)"

[Scott%20Klein](http://twitter.com/kleinmatic)
([ProPublica)](http://www.propublica.org/)
[view](http://twitter.com/kleinmatic/statuses/14253552041)

"I just got to know old data all over again using Freebase Gridworks"

[Rich%20Vázquez](http://twitter.com/drapetomaniac)
([ImpactNews)](http://impactnews.com/)
[view](http://twitter.com/drapetomaniac/statuses/13756191891)

"@fbase's Gridworks has taken a good day and made it truly epic.
<http://bit.ly/c9Ttdv> Thanks for the boost guys."

[Christopher%20Groskopf](http://twitter.com/onyxfish),
[view%20tweet](http://twitter.com/onyxfish/status/13741775910)

"Gridworks is a gamechanger, no doubt\! RT @stefanomaz theory and
practice behind clustering in Gridworks <http://bit.ly/b0EyyB>"

[Jon%20Voss](http://twitter.com/LookBackMaps),
[view%20tweet](http://twitter.com/LookBackMaps/statuses/14338170478)

"I'm done here - after google (prediction-api) and metaweb (gridworks)
have been through there is nothing left to do."

[Gunnar%20Grimnes](http://twitter.com/gromgull),
[view%20tweet](http://twitter.com/gromgull/statuses/14348680235)

"<http://bit.ly/93pTSL> @judell "Freebase Gridworks will make you weep
with joy' (via @rdmpage) Wow\! Why wasn't this invented earlier"

[Peter%20Desmet](http://twitter.com/peterdesmet),
[view%20tweet](http://twitter.com/peterdesmet/status/13791307008)

"Freebase releases Gridworks 1.0 This is a the most useful tool I have
seen in a long time. <http://bit.ly/c8emxf>"

[Zac%20Witte](http://twitter.com/zacwitte),
[view%20tweet](http://twitter.com/zacwitte/status/13746563538)

"I've had so much fun alpha testing Freebase Gridworks. It is very much
the powertool for working with datasets I had been waiting for."

[Raymond%20Yee](http://twitter.com/rdhyee),
[view%20tweet](http://twitter.com/rdhyee/status/13404067036)

"@JeniT freebase gridworks proves you don't need RDF to be semantic :)
used it heavily over the weekend"

[Rob%20McKinnon](http://twitter.com/delineator),
[view%20tweet](http://twitter.com/delineator/statuses/14630650064)

"Using gridworks to clean up @farmsubsidy data. This is fun :)"

[Sym%20Roe](http://twitter.com/symroe),
[view%20tweet](http://twitter.com/symroe/statuses/14619696804)

"I \<3 scatterplot matrix and scatterfacets\! Don't know what they are?
Watch this gridworks screencast: <http://vimeo.com/11854491> "

[Jeanne%20Kramer-Smyth](http://twitter.com/spellboundblog),
[view%20tweet](http://twitter.com/spellboundblog/statuses/14264336609)

"Freebase Gridworks very impressive <http://bit.ly/cjx3ZX>, covers many
features we want in pub workbench (discovered via @JeniT)"
[Dave%20Reynolds](http://twitter.com/der42),
[view%20tweet](http://twitter.com/der42/status/11188314594)

"Gridworks looks amazing. Great UI for data cleansing, record linkage,
schema mapping. We need this for \#linkeddata\! <http://bit.ly/diAw4a> "

[Richard%20Cyganiak](http://twitter.com/cygri),
[view%20tweet](http://twitter.com/cygri/status/11492299667)

"Well, @stefanomaz. Haven't got as excited as I just did from Gridworks
since… Cocoon in 1999\!\!\! I'm OLD\! Congrats\!"

[Vincent%20Olivier](http://twitter.com/Vincent\_Olivier),
[view%20tweet](http://twitter.com/Vincent\_Olivier/status/14202326806)

"Getting over excited about using Freebase Gridworks for data cleanup,
munging, and maybe, just maybe easy generation... <http://ff.im/k73kf> "

[Cameron%20Neylon](http://twitter.com/CameronNeylon),
[view%20tweet](http://twitter.com/CameronNeylon/statuses/13731457745)

"Judging by the number of people searching for Freebase Gridworks and
landing on my mini-review, it's getting some use. <http://bit.ly/cvsSTh>
"

[Anthony%20DeBarros](http://twitter.com/AnthonyDB),
[view%20tweet](http://twitter.com/AnthonyDB/status/17620349275)

# Extensions

**[DCat](http://lab.linkeddata.deri.ie/2010/dcat/)**

\- Fadi Maali, [Richard%20Cyganiak](http://richard.cyganiak.de/), and
Vassilios Peristeras ([DERI)](http://www.deri.ie/)

**\[<https://github.com/newsapps/refine-stats> Refine Stats\]**

\- [Joe%20Germuska](http://twitter.com/joegermuska) and
[Christopher%20Groskopf](http://twitter.com/onyxfish)

# Tutorials/Reviews

[Anthony%20DeBarros](http://www.anthonydebarros.com/about/)
([USA%20Today)](http://www.usatoday.com/) -
**[Test%20Drive:%20Freebase%20Gridworks%201.1](http://www.anthonydebarros.com/2010/06/06/freebase-gridworks-1-1/)**

[Jay%20Luker](http://www.linkedin.com/in/jayluker)
([Harvard-Smithsonian%20Center%20for%20Astrophysics)](http://www.cfa.harvard.edu/)
-
**[Exploring%20Astronomy%20Dataset%20Links%20with%20GridWorks](http://blog.reallywow.com/archives/135)**

[Paolo%20Ciccarese](http://www.linkedin.com/in/paolociccarese)
([Harvard%20Medical%20School)](http://hms.harvard.edu/) -
**[First%20Steps%20with%20Gridworks%20-%20part%201](http://hcklab.blogspot.com/2010/04/first-steps-with-gridworks-1.html)**
([part%202](http://hcklab.blogspot.com/2010/04/first-steps-with-gridworks-2.html),
[part3)](http://hcklab.blogspot.com/2010/04/first-steps-with-gridworks-3.html)

[Jeff%20Larson](http://www.propublica.org/site/author/jeff\_larson)
([ProPublica)](http://www.propublica.org/) -
**[The%20Rainbow%20Connection:%20How%20We%20Made%20Our%20CDO%20Connections%20Graphic](http://www.propublica.org/nerds/item/the-rainbow-connection-how-we-made-our-cdo-connections-graphic)**

[Where%20to%20sleep%20in%20Turin:%20un%20esempio%20d’uso%20dei%20raw%20data%20piemontesi%20con%20Google%20Refine%20e%20SIMILE%20Exhibit](http://blog.spaziogis.it/2010/10/29/where-to-sleep-in-turin-un-esempio-d-uso-dei-raw-data-piemontesi-con-google-refine-e-simile-exhibit/)

[Tim%20Davies](http://www.timdavies.org.uk/) -
**[Sliced%20and%20diced%20aid%20data%20with%20google%20refine](http://www.opendatacookbook.net/wiki/recipe/sliced\_and\_diced\_aid\_data\_with\_google\_refine)**
