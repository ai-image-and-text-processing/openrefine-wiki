*Issue Tracking*

Issue Tracking is only for reporting specific bugs and requesting
specific features. If you just don't know how to do something using
OpenRefine, or want to discuss some ideas, please

  - try [[Getting Started]]
  - post to our [OpenRefine mailing list](http://groups.google.com/group/openrefine/).

If you really want to file a bug or request a feature, see [[issues]].

Thank you!
