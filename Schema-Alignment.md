In OpenRefine, Schema Alignment is a process that transforms the tabular data in a project to the data format used by a knowledge base. A schema is a data structure that maps each row to a set of triples, statements or other records, depending on the target base. This functionality is provided by extensions or forks:

| Knowledge base | Extension or fork | Documentation | OpenRefine version |
|----------------|-----------------------------------------|---------------|--------------------|
| [Wikidata](https://www.wikidata.org/wiki/) | Wikidata extension (part of OpenRefine) | [Documentation](https://www.wikidata.org/wiki/Wikidata:Tools/OpenRefine/Editing) | 3.0+ |
| Any RDF store  | [RDF extension](https://github.com/stkenny/grefine-rdf-extension) | [Documentation](https://github.com/stkenny/grefine-rdf-extension/wiki) | 2.6+ |
| [Freebase](https://en.wikipedia.org/wiki/Freebase)       | Freebase extension (discontinued) | [Documentation](https://github.com/OpenRefine/OpenRefine/wiki/Freebase-Schema-Alignment) | 2.0-2.6 |
| [GOKb](https://gokb.org/)           | [GOKb extension](https://openlibraryenvironment.atlassian.net/wiki/spaces/GOKB/pages/656218/Install+the+GOKb+Extension) | [Documentation](https://openlibraryenvironment.atlassian.net/wiki/spaces/GOKB/pages/656219/Tutorial+GOKb+Data+Ingest+Using+OpenRefine) | 2.5+ |
| [GraphDB](http://graphdb.ontotext.com/)       | [OntoRefine](https://ontotext.com/tabular-data-rdf-graphdb/) | [Documentation](http://graphdb.ontotext.com/documentation/enterprise/loading-data-using-ontorefine.html) | 2.7+ |
