## New features

* A new menu for joining (concatenating) columns has been added ([#2109](https://github.com/OpenRefine/OpenRefine/issues/2109))
* Commonly used fields in dialogs now have autofocus ([#2130](https://github.com/OpenRefine/OpenRefine/issues/2130))
* The Wikidata extension now supports adding dates with custom calendars ([#2136](https://github.com/OpenRefine/OpenRefine/issues/2136))
* Calling reconciliation services via CORS is now supported ([#2260](https://github.com/OpenRefine/OpenRefine/issues/2260))
* All columns can be blanked down or filled down at once ([#2280](https://github.com/OpenRefine/OpenRefine/issues/2280))
* New "Blank values per column" and "Blank records per column" facets were added ([#2220](https://github.com/OpenRefine/OpenRefine/issues/2220))

## Vulnerabilities
* A cross-site scripting vulnerability in the database extension was fixed ([#2151](https://github.com/OpenRefine/OpenRefine/pull/2151))
* [Cross-Site Request Forgery (CSRF)](https://en.wikipedia.org/wiki/Cross-site_request_forgery) protection was added to POST API endpoints. If you rely on OpenRefine's server API you will need to adapt your calls accordingly, see below ([#2164](https://github.com/OpenRefine/OpenRefine/issues/2164))

## Bug fixes
* The scatterplot facet was fixed ([#2117](https://github.com/OpenRefine/OpenRefine/issues/2117))
* Clustering no longer returns clusters with single values ([#2152](https://github.com/OpenRefine/OpenRefine/issues/2152))
* Changing the UI language works again ([#2158](https://github.com/OpenRefine/OpenRefine/issues/2158))
* A bug in the reconciliation dialog which discarded property identifiers when a custom type was selected after them was fixed ([#2166](https://github.com/OpenRefine/OpenRefine/issues/2166))
* OpenRefine now sends the selected reconciliation type as part of the suggest API instead of the default Freebase type ([#2168](https://github.com/OpenRefine/OpenRefine/issues/2168))
* The data extension dialog uses the correct view URL pattern to generate URLs for reconciled entities ([#2177](https://github.com/OpenRefine/OpenRefine/issues/2177))
* Spell checking in expression editors has been disabled ([#1970](https://github.com/OpenRefine/OpenRefine/issues/1970))
* Layout of the table header for more verbose languages has been fixed ([#2183](https://github.com/OpenRefine/OpenRefine/issues/2183))
* OpenRefine incorrectly saved to Wikidata statements with a value over 2,147,483,647 ([#2244](https://github.com/OpenRefine/OpenRefine/issues/2244))
* Creation of wikidata items pointing to other new wikidata items was fixed ([#2206](https://github.com/OpenRefine/OpenRefine/issues/2206))
* Wikidata editing is more robust to network failures ([#2211](https://github.com/OpenRefine/OpenRefine/issues/2211))
* The localization fallback in the UI was fixed ([#2209](https://github.com/OpenRefine/OpenRefine/issues/2209), [#1350](https://github.com/OpenRefine/OpenRefine/issues/1350))
* Export to Google Sheets was fixed ([#2123](https://github.com/OpenRefine/OpenRefine/issues/2123))
* Import of private Google Sheets was fixed ([#2223](https://github.com/OpenRefine/OpenRefine/issues/2223))
* Handling of large quantity values in Wikidata schema was fixed ([#2244](https://github.com/OpenRefine/OpenRefine/issues/2244))
* Wikidata login is correctly remembered again in the same session ([#2228](https://github.com/OpenRefine/OpenRefine/issues/2228))
* The localization of the UI was fixed for cases where no language has been set in the preferences ([#2227](https://github.com/OpenRefine/OpenRefine/issues/2227))
* Support for Google Fusion tables has been removed ([#2113](https://github.com/OpenRefine/OpenRefine/issues/2113))
* A bug in the expression history was fixed ([#2086](https://github.com/OpenRefine/OpenRefine/issues/2086))
* An alert related to reconciliation could pop up when working with unrelated features, this has been fixed ([#1665](https://github.com/OpenRefine/OpenRefine/issues/1665))
* A bug in XLSX export for cells containing URLs was fixed ([#2213](https://github.com/OpenRefine/OpenRefine/issues/2213))
* The "Sort" button is now translatable. ([#2233](https://github.com/OpenRefine/OpenRefine/issues/2233))
* The translation of the interface was improved via [Weblate](https://hosted.weblate.org/engage/openrefine/)

## For developers
* HTTP calls in data extension tests are now mocked ([#1904](https://github.com/OpenRefine/OpenRefine/issues/1904))
* Tests were reogranized into their correct packages (the same as the class being tested) ([#2133](https://github.com/OpenRefine/OpenRefine/issues/2133))
* Jackson has been updated to the latest version 2.9.10 ([#2181](https://github.com/OpenRefine/OpenRefine/pull/2181))
* The embedded Maven, downloaded when no Maven is available on the system, has been updated ([#2286](https://github.com/OpenRefine/OpenRefine/issues/2286))
* A contributor code of conduct has been adopted ([#2102](https://github.com/OpenRefine/OpenRefine/pull/2102))

## CSRF protection changes
To protect users from alteration of their workspace by other websites and other exploits, we have added [Cross-Site Request Forgery (CSRF)](https://en.wikipedia.org/wiki/Cross-site_request_forgery) protection to API commands which use the POST method. This means that before calling these POST methods, you need to obtain a CSRF token using the dedicated command:

`GET command/core/get-csrf-token`

This returns a token in a JSON response:

```
{
    "token": "WdzF1oUsK3KyIQYaWLJVar35mHndRrO7"
}
```

This token must then be added as `csrf_token` parameter to any POST request which performs a state change in the backend or perform other side-effects:

```
POST command/core/open-workspace-dir
csrf_token=WdzF1oUsK3KyIQYaWLJVar35mHndRrO7
```

This applies to the following commands (where the `csrf_token` can be supplied either as GET or POST parameter):
* `add-column-by-fetching-urls`;
* `add-column`;
* `apply-operations`;
* `annotate-one-row`;
* `annotate-rows`;
* `blank-down`;
* `cancel-processes`;
* `cancel-importing-job`;
* `create-importing-job`;
* `delete-project`;
* `denormalize`;
* `edit-one-cell`;
* `extend-data`;
* `fill-down`;
* `guess-types-of-columns`;
* `join-multi-value-cells`;
* `key-value-columnize`;
* `log-expression`;
* `mass-edit`;
* `move-column`;
* `open-workspace-dir`;
* `preview-expression`;
* `preview-extend-data`;
* `reconcile`;
* `recon-clear-one-cell`;
* `recon-clear-similar-cells`;
* `recon-copy-across-columns`;
* `recon-discard-judgments`;
* `recon-judge-one-cell`;
* `recon-judge-similar-cells`;
* `recon-mark-new-topics`;
* `recon-match-best-candidates`;
* `recon-match-specific-topic`;
* `recon-use-values-as-identifiers`;
* `rename-project`;
* `remove-column`;
* `remove-rows`;
* `reorder-columns`;
* `reorder-rows`;
* `set-preference`;
* `set-project-metadata` (previously known as `set-metaData`);
* `set-project-tags`;
* `split-column`;
* `split-multi-value-cells`;
* `text-transform`;
* `toggle-starred-expression`;
* `transpose-columns-into-rows`;
* `transpose-rows-into-columns`;
* `undo-redo`.


The following commands only accept `csrf_token` as GET parameter:
* `create-project`;
* `importing-controller` and subcommands;
* `import-project`.

The following commands can still be used with POST but do not require CSRF tokens (although supplying one will not fail), as they do not incur a state change in the backend:
* `compute-clusters`;
* `compute-facets`;
* `export-project`;
* `export-rows`;
* `get-all-preferences`;
* `get-importing-configuration`;
* `get-importing-job-status`;
* `get-rows`;
* `load-language`.

The following commands used to be accessible with both POST and GET, they are now GET-only:
* `get-languages`.