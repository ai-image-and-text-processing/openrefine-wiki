*Array functions supported by the [[General Refine Expression Language]] (GREL)*

See also: [[GREL Functions]]

### length(array a)

Returns the length of array `a`.

### slice(array a, number from, optional number to)

Returns the sub-array of `a` from its index `from` up to but not
including index `to`. If `to` is omitted, it is understood to be the end
of the array `a`. For example, `slice([ 0, 1, 2, 3, 4], 1, 3)` returns
`[ 1, 2 ]`, and `slice([ 0, 1, 2, 3, 4], 1)` returns `[ 1, 2, 3, 4 ]`.

slicing an array gives you another array.

### get(array a, number or string from, optional number to)

returns: Depends on actual arguments.

If o has fields, returns the field named 'from' of o. If o is an array,
returns o\[from, to\]. If o is a string, returns o.substring(from, to)

For example,

`[1,2,3,4].get(2)` returns the value `3`

`["A","B","C","D","E"].get(1,4)` returns the value `[ "B", "C", "D" ]`

Note: "E" is not returned in the 2nd example because the optional `to`
argument is not included in the returned output, this is the same as
`slice()` handling.

### reverse(array a)

Reverses array `a`. For example, `reverse([ 0, 1, 2, 3])` returns the
array `[ 3, 2, 1, 0 ]`.

### sort(array a)

Sorts array `a` in ascending order. For example, `sort([ 2, 1, 0, 3])`
returns the array `[ 0, 1, 2, 3 ]`.

### sum(array a)

Return the sum of the numbers in the array `a`. For example,
`sum([ 2, 1, 0, 3])` returns `6`.

### join(array a, string sep)

Returns the string obtained by joining the array `a` with the separator
`sep`. For example, `join([ "foo", "bar", "baz" ], ";")` returns the
string `foo;bar;baz`.

### uniques(array a)

Returns array `a` with duplicates removed. For example,
`uniques([ 2, 1, 2, 0, 1, 4, 3])` returns the array `[ 0, 1, 2, 3, 4 ]`.
