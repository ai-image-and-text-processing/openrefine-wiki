*Other functions supported by the [[General Refine Expression Language]] (GREL)*

See also: [[GREL Functions]]

### type(o)

Returns the type of `o`, such as `undefined`, `string`, `number`, etc.

### hasField(o, string name)

Returns a boolean indicating whether `o` has a field called `name`. For
example, `cell.hasField("value")` always returns `true`, as every cell
has a `value` field.

### coalesce(o1, o2, o3, ...)

Returns the first non-null from a series of values (the list of
`objects` o1, o2, o3 ...).

Example of use is: `coalesce(value,"")`

This would return an empty string "" if the value was null, but
otherwise, return the value.

*OpenRefine 3.0 onwards only*

### jsonize(value)

Quotes a value as a JSON literal value.

### parseJson(string s)

Parses `s` as JSON. `get` can then be used with `parseJson`, e.g.,

    parseJson(" { 'a' : 1 } ").get("a")

returns 1.

To get all instances from a JSON array called "keywords" having the same
object string name of "text", combine with the `forEach()` function to
iterate over the array, e.g.,

JSON:

    {
       "status":"OK",
       "url":"",
       "language":"english",
       "keywords":[
          {
             "text":"York en route",
             "relevance":"0.974363"
          },
          {
             "text":"Anthony Eden",
             "relevance":"0.814394"
          },
          {
             "text":"President Eisenhower",
             "relevance":"0.700189"
          }
       ]
    }

GREL expression:

    forEach(value.parseJson().keywords,v,v.text).join(":::")

will output like this:

`York en route:::Anthony Eden:::President Eisenhower`

### cross(cell c, string projectName, string columnName)

Returns an array of zero or more rows in the project `projectName` for
which the cells in their column `columnName` have the same content as
cell `c` (similar to a lookup).

**NOTE: There is also a VIB-BITS OpenRefine extension that automates
some of the GREL `cross()` functionality for ease of use with its "Add
column(s) from other projects..." function.**

  - <https://groups.google.com/forum/#!topic/openrefine/unN6dQsyYUs>
  - <https://www.bits.vib.be/index.php/software-overview/openrefine>

**NOTE: If you don't get matches when using cross(), then you might need
to first do a few things, such as**

  - trim() your key column before doing cross()
  - Deduplicate values in your key column if necessary

OK, with that out the way, let's show you an example of how to use the
cross() function. Consider 2 projects with the following data:

**My Address Book**

| friend | address             |
| ------ | ------------------- |
| john   | 120 Main St.        |
| mary   | 50 Broadway Ave.    |
| anne   | 17 Morning Crescent |

**Christmas Gifts**

| gift  | recipient |
| ----- | --------- |
| lamp  | mary      |
| clock | john      |

Now in the project "Christmas Gifts", we want to add a column containing
the addresses of the recipients, which are in the project "My Address
Book". So we can invoke the command `Add column based on this column` on
the "recipient" column in "Christmas Gifts" and enter this expression:

``` 
  cell.cross("My Address Book", "friend")[0].cells["address"].value
```

When that command is applied, the result is

**Christmas Gifts**

| gift  | recipient | address          |
| ----- | --------- | ---------------- |
| lamp  | mary      | 50 Broadway Ave. |
| clock | john      | 120 Main St.     |

#### Advanced cross() usage

  - To perform a cross or lookup between 2 columns (B and C) in the same
    project, you can create a new column "match" based on the expression
    using `forEach()` , retrieve the value in A when they match, and
    then `join()` the produced arrays, as in this example:

Original:

    A   B          C 
    098 11079851   11079851
    110 11089385   25853201
    118 11089385   22412115
    798 11079851   22412115

GREL:

    forEach(cross(cell,"my_project","C"),v,v.cells["A"].value).join(',')

Result:

    A   B          match      C 
    098 11079851              11079851
    110 11089385   098        25853201
    118 11089385   118,798    22412115
    798 11079851              22412115

  - Combine with the `forEach()` function to iterate over the array and
    the `forNonBlank()` function to prevent errors caused by null values
    present in the looked up column, e.g.

<!-- end list -->

``` 
  forEach(cell.cross("My Address Book", "friend"),r,forNonBlank(r.cells["address"].value,v,v,"")).join("|")
```

_Also note that felixlohmeier has written a nice enhanced cross function that can do a cross with repeating values in the same cell (i.e. comma delimiter list)._ [https://gist.github.com/felixlohmeier/a5a893190e4aa8e26091664908d04e20](https://gist.github.com/felixlohmeier/a5a893190e4aa8e26091664908d04e20)

### facetCount(choiceValue, string facetExpression, string columnName)

Returns the facet count corresponding to the given choice value

| gift  | recipient | price |
| ----- | --------- | ----- |
| lamp  | mary      | 20    |
| clock | john      | 54    |
| watch | amit      | 80    |
| clock | claire    | 62    |

If we wanted to show how many of each gift we had given, we could use
the following expression:

``` 
  facetCount(value, "value", "gift")
```

This would then complete the table as so:

| gift  | recipient | price | count |
| ----- | --------- | ----- | ----- |
| lamp  | mary      | 20    | 1     |
| clock | john      | 54    | 2     |
| watch | amit      | 80    | 1     |
| clock | claire    | 62    | 2     |

## Jsoup XML and HTML parsing functions

-----

A tutorial of the below HTML functions is shown [ StrippingHTML
](here.%20)

### parseHtml(string s)

returns a full HTML document and adds any missing closing tags.

You can then extract or `.select()` which portions of the HTML document
you need for further splitting, partitioning, etc.

An example of extracting all table rows `<tr>` from a `<div>` using
`parseHtml().select()` together is described more in [ StrippingHTML
](Extract%20HTML%20attributes,%20text,%20links%20with%20integrated%20GREL%20commands%20).

### parseXml(string s)

returns a full XML document and adds any missing closing tags.

You can then extract or `.select()` which portions of the XML document
you need for further splitting, partitioning, etc.

### select(Element e, String s)

returns an element from an HTML doc element using selector syntax.

Example:

`value.parseHtml().select("div#content")[0].select("tr").toString()`

`value.parseXml().select("div[id='content']")[0].select("tr").toString()`

You can even parseHtml more than once when needed for deeply embedded
data, like CDATA HTML, etc.

`value.parseHtml().select("description")[0].htmlText().parseHtml().select("tr:contains(satellite)")`

This function can be used with most of the Jsoup selector syntax as
documented here:
<http://jsoup.org/cookbook/extracting-data/selector-syntax>

### htmlAttr(Element e, String s)

returns a value from an attribute on an HTML Element.

### xmlAttr(Element e, String s)

returns a value from an attribute on an XML Element.

### htmlText(Element e)

returns the text from within an HTML element (including all child
elements).

### xmlText(Element e)

returns the text from within an XML element (including all child
elements).

### innerHtml(Element e)

returns the innerHtml of an HTML element. This will include text and
elements within the element selected

### innerXml(Element e)

returns the innerXml of an XML element. This will \*only\* include
nested elements from within the selected element. Text from the selected
element level will not be included.

### outerHtml(Element e)

returns the outerHtml of an HTML element. (Note: outerHtml Indicates the
rendered text and HTML tags including the start and end tags, of the
current element.)

### ownText(Element e)

Gets the text owned by the selected XML or HTML element only; does not
get the combined text of all children.
