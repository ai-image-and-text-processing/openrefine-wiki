*List of sample datasets that you can use to try out OpenRefine.*

Here are a few sample data sets to get you started on understanding how
to use OpenRefine

  - [AWS%20Public%20Datasets%20at%20Amazon](https://aws.amazon.com/public-datasets/)
  - [Million%20Song%20Dataset](http://labrosa.ee.columbia.edu/millionsong/)
  - [TRADE%20REPOSITORY%20PUBLIC%20DATA](http://www.lseg.com/markets-products-and-services/post-trade-services/unavista/unavista-solutions/emir-trade-repository/trade-repository-public-data)
  - [Louisiana%20elected%20officials](http://electionstatistics.sos.la.gov/Data/Elected\_Officials/ElectedOfficials.xls)
    - For OpenRefine versions up to 2.0, you must use Excel itself to
    convert the .xls file to a .tsv file first because the Java library
    for dealing with .xls files doesn't like this particular .xls file.
    OpenRefine versions 2.1 and above will successfully load the .xls
    directly
  - [Clean%20Air%20Status%20and%20Trends%20Network%20\(CASTNET\)%20raw%20data](http://java.epa.gov/castnet/clearsession.do)
  - [Federal%20Advisory%20Committee%20Act%20\(FACA\)%20Committee%20Member%20Lists](https://gsageo.force.com/FACA/apex/FACADatasets)
  - [U.S.%20Overseas%20Loans%20and%20Grants%20\(Greenbook\)](http://catalog.data.gov/dataset/us-overseas-loans-and-grants-greenbook-usaid-1554)
    - 2K rows
  - [Hurricane%20Sandy-related%20NYC%20311%20calls](https://data.cityofnewyork.us/Social-Services/sandyrelated/fs5z-tpv4)
    - 84K rows, 53 columns
  - [U.S.%20Census%20Geography%20and%20Environment%20data%20](http://www.census.gov/library/publications/2011/compendia/statab/131ed/geography-environment.html)
